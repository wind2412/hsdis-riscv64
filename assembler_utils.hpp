//
// Created by wind2412 on 2022/5/9.
//

#ifndef HSDIS_RISCV64_ASSEMBLER_UTILS_HPP
#define HSDIS_RISCV64_ASSEMBLER_UTILS_HPP

#include <stdio.h>
#include <cstdlib>
#include <cassert>
#include <inttypes.h>

#define RED_PRINT    "\033[31m"
#define GREEN_PRINT  "\033[32m"
#define YELLOW_PRINT "\033[33m"
#define BLUE_PRINT   "\033[34m"
#define BLUE_PRINT   "\033[34m"
#define AOI_PRINT    "\033[46m"
#define RESET_COLOR  "\033[0m"

typedef unsigned char u_char;
typedef u_char*       address;
const int wordSize           = sizeof(char*);
#define INTPTR_FORMAT "0x%016" PRIxPTR

intptr_t p2i(const void * p) {
  return (intptr_t) p;
}

typedef unsigned long(*func)();
typedef void(*test_func)();
typedef void(*set_args_func)(int argc, char *argv[]);

#define DEFAULT_MT_CNT            2

#define CODE_SZ                   100
#define CODE_SZ_200               200
const size_t K                  = 1024;
const size_t M                  = K*K;
const size_t G                  = M*K;
#define GB 0x40000000

class RegisterImpl {
public:
  enum {
      // C-Ext: float registers in the range of [f8~f15] are correspond for RVC. Please see Table 16.2 in spec.
      compressed_register_base = 8,
      compressed_register_top  = 15,
  };
  int encoding_nocheck() const                 { return (intptr_t)this; }
  int compressed_encoding_nocheck() const      { return ((intptr_t)this - compressed_register_base); }
};
typedef RegisterImpl* Register;

#define x0         (Register)0
#define zr         (Register)x0
#define x1         (Register)1
#define lr         (Register)x1
#define ra         (Register)lr
#define x2         (Register)2
#define sp         (Register)x2
#define x3         (Register)3
#define x4         (Register)4
#define x5         (Register)5
#define t0         (Register)x5
#define x6         (Register)6
#define t1         (Register)x6
#define x7         (Register)7
#define t2         (Register)x7
#define x8         (Register)8
#define s0         (Register)x8
#define fp         (Register)x8
#define x9         (Register)9
#define x10        (Register)10
#define a0         (Register)x10
#define x11        (Register)11
#define a1         (Register)x11
#define x12        (Register)12
#define a2         (Register)x12
#define x13        (Register)13
#define x14        (Register)14
#define x15        (Register)15
#define x16        (Register)16
#define x17        (Register)17
#define x18        (Register)18
#define x19        (Register)19
#define x20        (Register)20
#define x21        (Register)21
#define x22        (Register)22
#define x23        (Register)23
#define x24        (Register)24
#define x25        (Register)25
#define x26        (Register)26
#define x27        (Register)27
#define x28        (Register)28
#define t3         (Register)x28
#define x29        (Register)29
#define t4         (Register)x29
#define x30        (Register)30
#define t5         (Register)x30
#define x31        (Register)31
#define t6         (Register)x31

address hint_callee      = address(   0x100000000);
address hint_caller_near = hint_callee - 100 * K;
address hint_caller_far  = hint_callee - 1   * G;
address hint_caller_far_trampoline  = hint_callee - 2   * G;

const intptr_t NoBits     =  0; // no bits set in a word
const intptr_t OneBit     =  1; // only right_most bit set in a word
const int LogBitsPerByte     = 3;
const int LogBytesPerWord    = 3;
const int LogBitsPerWord     = LogBitsPerByte + LogBytesPerWord;
const int BitsPerByte        = 1 << LogBitsPerByte;
const int BitsPerWord        = 1 << LogBitsPerWord;
#define nth_bit(n)        (((n) >= BitsPerWord) ? 0 : (OneBit << (n)))
#define right_n_bits(n)   (nth_bit(n) - 1)

template <typename T2, typename T1>
T2 checked_cast(T1 thing) {
  T2 result = static_cast<T2>(thing);
  assert(static_cast<T1>(result) == thing && "must be");
  return result;
}

// abs methods which cannot overflow and so are well-defined across
// the entire domain of integer types.
unsigned int uabs(unsigned int n) {
  union {
      unsigned int result;
      int value;
  };
  result = n;
  if (value < 0) result = 0-result;
  return result;
}
unsigned long uabs(unsigned long n) {
  union {
      unsigned long result;
      long value;
  };
  result = n;
  if (value < 0) result = 0-result;
  return result;
}
static unsigned long uabs(long n) { return uabs((unsigned long)n); }
static unsigned int uabs(int n) { return uabs((unsigned int)n); }

typedef intptr_t  intx;
typedef uintptr_t uintx;

inline bool is_odd (intx x) { return x & 1;      }
inline bool is_even(intx x) { return !is_odd(x); }

Register as_Register(int encoding) {
  return (Register)(intptr_t) encoding;
}

template <bool condition, typename TrueType, typename FalseType>
struct Conditional {
    typedef TrueType type;
};

template <typename T>
inline unsigned population_count(T x) {
  // We need to take care with implicit integer promotion when dealing with
  // integers < 32-bit. We chose to do this by explicitly widening constants
  // to unsigned
  typedef typename Conditional<(sizeof(T) < sizeof(unsigned)), unsigned, T>::type P;
  const T all = ~T(0);           // 0xFF..FF
  const P fives = all/3;         // 0x55..55
  const P threes = (all/15) * 3; // 0x33..33
  const P z_ones = all/255;      // 0x0101..01
  const P z_effs = z_ones * 15;  // 0x0F0F..0F
  P r = x;
  r -= ((r >> 1) & fives);
  r = (r & threes) + ((r >> 2) & threes);
  r = ((r + (r >> 4)) & z_effs) * z_ones;
  // The preceding multiply by z_ones is the only place where the intermediate
  // calculations can exceed the range of T. We need to discard any such excess
  // before the right-shift, hence the conversion back to T.
  return static_cast<T>(r) >> (((sizeof(T) - 1) * BitsPerByte));
}

template <class RegImpl> class RegSetIterator;
template <class RegImpl> class ReverseRegSetIterator;

// A set of registers
template <class RegImpl>
class AbstractRegSet {
  uint32_t _bitset;

  AbstractRegSet(uint32_t bitset) : _bitset(bitset) { }

public:

  AbstractRegSet() : _bitset(0) { }

  AbstractRegSet(RegImpl r1) : _bitset(1 << r1->encoding_nocheck()) { }

  AbstractRegSet operator+(const AbstractRegSet aSet) const {
    AbstractRegSet result(_bitset | aSet._bitset);
    return result;
  }

  AbstractRegSet operator-(const AbstractRegSet aSet) const {
    AbstractRegSet result(_bitset & ~aSet._bitset);
    return result;
  }

  AbstractRegSet &operator+=(const AbstractRegSet aSet) {
    *this = *this + aSet;
    return *this;
  }

  AbstractRegSet &operator-=(const AbstractRegSet aSet) {
    *this = *this - aSet;
    return *this;
  }

  static AbstractRegSet of(RegImpl r1) {
    return AbstractRegSet(r1);
  }

  static AbstractRegSet of(RegImpl r1, RegImpl r2) {
    return of(r1) + r2;
  }

  static AbstractRegSet of(RegImpl r1, RegImpl r2, RegImpl r3) {
    return of(r1, r2) + r3;
  }

  static AbstractRegSet of(RegImpl r1, RegImpl r2, RegImpl r3, RegImpl r4) {
    return of(r1, r2, r3) + r4;
  }

  static AbstractRegSet range(RegImpl start, RegImpl end) {
    assert(start <= end && "must be");
    uint32_t bits = ~0;
    bits <<= start->encoding_nocheck();
    bits <<= 31 - end->encoding_nocheck();
    bits >>= 31 - end->encoding_nocheck();

    return AbstractRegSet(bits);
  }

  uint size() const { return population_count(_bitset); }

  uint32_t bits() const { return _bitset; }

private:

  RegImpl first();
  RegImpl last();

public:

  friend class RegSetIterator<RegImpl>;
  friend class ReverseRegSetIterator<RegImpl>;

  RegSetIterator<RegImpl> begin();
  ReverseRegSetIterator<RegImpl> rbegin();
};

template <class RegImpl>
class RegSetIterator {
  AbstractRegSet<RegImpl> _regs;

public:
  RegSetIterator(AbstractRegSet<RegImpl> x): _regs(x) {}
  RegSetIterator(const RegSetIterator& mit) : _regs(mit._regs) {}

  RegSetIterator& operator++() {
    RegImpl r = _regs.first();
    if (r->is_valid())
      _regs -= r;
    return *this;
  }

  bool operator==(const RegSetIterator& rhs) const {
    return _regs.bits() == rhs._regs.bits();
  }
  bool operator!=(const RegSetIterator& rhs) const {
    return ! (rhs == *this);
  }

  RegImpl operator*() {
    return _regs.first();
  }
};

template <class RegImpl>
inline RegSetIterator<RegImpl> AbstractRegSet<RegImpl>::begin() {
return RegSetIterator<RegImpl>(*this);
}

template <class RegImpl>
class ReverseRegSetIterator {
  AbstractRegSet<RegImpl> _regs;

public:
  ReverseRegSetIterator(AbstractRegSet<RegImpl> x): _regs(x) {}
  ReverseRegSetIterator(const ReverseRegSetIterator& mit) : _regs(mit._regs) {}

  ReverseRegSetIterator& operator++() {
    RegImpl r = _regs.last();
    if (r->is_valid())
      _regs -= r;
    return *this;
  }

  bool operator==(const ReverseRegSetIterator& rhs) const {
    return _regs.bits() == rhs._regs.bits();
  }
  bool operator!=(const ReverseRegSetIterator& rhs) const {
    return ! (rhs == *this);
  }

  RegImpl operator*() {
    return _regs.last();
  }
};

template <class RegImpl>
inline ReverseRegSetIterator<RegImpl> AbstractRegSet<RegImpl>::rbegin() {
  return ReverseRegSetIterator<RegImpl>(*this);
}

typedef AbstractRegSet<Register> RegSet;

#endif //HSDIS_RISCV64_ASSEMBLER_UTILS_HPP
