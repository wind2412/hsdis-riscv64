//
// Created by wind2412 on 2022/5/9.
//

#ifndef HSDIS_RISCV64_DISASSEMBLER_HPP
#define HSDIS_RISCV64_DISASSEMBLER_HPP

#include "assembler_utils.hpp"
#include <dlfcn.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <iostream>
#include <cstdio>
#include <cstdarg>

int dis_asm_styled_fprintf (void *stream,
                            int style,
                            const char *format, ...)
{
  va_list args;

  va_start (args, format);
  vfprintf ((FILE *) stream, format, args);
  va_end (args);
  /* Something non -ve.  */
  return 0;
}

class Disassembler {
/* does the event match the tag, followed by a null, space, or slash? */
#define MATCH(event, tag) \
  (!strncmp(event, tag, sizeof(tag)-1) && \
   (!event[sizeof(tag)-1] || strchr(" /", event[sizeof(tag)-1])))
  static const char event_cookie[];
  static void* handle_event(void* cookie, const char* event, void* arg) {
    if (cookie != event_cookie)
      printf("*** bad event cookie %p != %p\n", cookie, event_cookie);

    if (MATCH(event, "insn")) {
      /* basic action for <insn>: */
      printf(" " INTPTR_FORMAT "\t", p2i(arg));
    } else if (MATCH(event, "/insn")) {
      // follow each complete insn by a nice newline
      printf("\n");
    } else if (MATCH(event, "mach")) {
      printf("Decoding for CPU '%s'\n", (char*) arg);
    } else if (MATCH(event, "addr")) {
      /* basic action for <addr/>: */
//    const char* name = lookup(arg);
//    if (name) {
//      printf("&%s (%p)", name, arg);
//      /* return non-null to notify hsdis not to print the addr */
//      return arg;
//    }

//      printf("<?>");
    }

    /* null return is always safe; can mean "I ignored it" */
    return NULL;
  }
private:
  static address _library;
  typedef void* (*decode_func_virtual)(uintptr_t start_va, uintptr_t end_va,
                                       unsigned char* buffer, uintptr_t length,
                                       void* (*event_callback)(void*, const char*, void*),
                                       void* event_stream,
                                       int (*printf_callback)(void*, const char*, ...),
                                       void* printf_stream,
                                       int (*printf_styled_callback)(void*, int, const char*, ...),
                                       const char* options,
                                       int newline);
  typedef int   (*decode_instructions_printf_callback_ftype) (void*, const char*, ...);
  typedef int   (*decode_instructions_printf_styled_callback_ftype) (void*, int, const char*, ...);
#define fprintf_callback (decode_instructions_printf_callback_ftype)&fprintf
#define fprintf_styled_callback (decode_instructions_printf_styled_callback_ftype)&dis_asm_styled_fprintf
  static Disassembler::decode_func_virtual _decode_instructions_virtual;
  static const char decode_instructions_virtual_name[];
  static const char options[];
private:
  static const char *hsdis_lib_path() {
    const char *lib = NULL;
#ifdef __linux__
    lib = "build/linux-riscv64/hsdis-riscv64.so";
#else
    assert(false);  // unimplemented
#endif

    char pwd[1024];
    getcwd(pwd, sizeof(pwd));
    printf("[pwd] %s\n", pwd);
    // need to be a full path or dlopen will fail.
    char buffer[1024];  // directly fork one for a lazy man as me.
    sprintf(buffer, "%s/%s", pwd, lib);
    printf("[searching] %s ...\n", buffer);

    return strdup(buffer);  // a good habit: allocating without a free
  }
  static void init() {
    const char *lib = hsdis_lib_path();
    _library = (address)dlopen(lib, RTLD_LAZY);
//    printf("%s\n", strerror(errno));
//    printf("%s\n", lib);
    assert(_library && "must have library");
    _decode_instructions_virtual = (decode_func_virtual)dlsym(_library, decode_instructions_virtual_name);
    assert(_decode_instructions_virtual && "must have function");
  }
public:

  static void decode(address start_address, size_t size, const char *name, const char *name_color = RED_PRINT) {
    // init. I don't want to draw this logic out.
    static bool initialized = false;
    if (!initialized) {
      init();
      initialized = true;
    }
    // decode.
    printf("\n------------------------ %s[%s]%s ------------------------\n", name_color, name, RESET_COLOR);
    printf(GREEN_PRINT);
    (*Disassembler::_decode_instructions_virtual)((uintptr_t)start_address, (uintptr_t)start_address+size,
                                                  start_address, size,
                                                  handle_event, (void*) event_cookie,
                                                  fprintf_callback, (void*) stdout,
                                                  fprintf_styled_callback,
            /* options */
//            NULL,
            // "numeric,no-aliases",  // Note: should not have extra spaces inside the char literal
            "numeric",
            0/*nice new line*/);
    printf(RESET_COLOR);
    printf("----------------------------------------------------------------\n");
    fflush(stdout);     // don't remove this! docker interactive mode sometimes needs this to get the right output when SIGSEGV happens (if we don't flush, sometimes no output).
  }
};
address Disassembler::_library = NULL;
Disassembler::decode_func_virtual Disassembler::_decode_instructions_virtual = NULL;
const char Disassembler::decode_instructions_virtual_name[] = "decode_instructions_virtual";
const char Disassembler::options[] = "no-aliases,numeric";
const char Disassembler::event_cookie[] = "event_cookie"; /* demo placeholder */



#endif //HSDIS_RISCV64_DISASSEMBLER_HPP
