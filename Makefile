#
# Copyright (c) 2008, 2017, Oracle and/or its affiliates. All rights reserved.
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
#
# This code is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 2 only, as
# published by the Free Software Foundation.
#
# This code is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# version 2 for more details (a copy is included in the LICENSE file that
# accompanied this code).
#
# You should have received a copy of the GNU General Public License version
# 2 along with this work; if not, write to the Free Software Foundation,
# Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
# or visit www.oracle.com if you need additional information or have any
# questions.
#
#

# Single gnu makefile for solaris, linux and windows (windows requires cygwin and mingw)

# Default arch; it is changed below as needed.
ARCH		= riscv64 # FIXME: workaround only for riscv
OS		= $(shell uname)
AR		= riscv64-unknown-linux-gnu-ar # FIXME: workaround only for riscv

## OS = Linux ##
ifeq		($(OS),Linux)
CPU             = riscv64
ARCH1=$(CPU:x86_64=amd64)
ARCH2=$(ARCH1:i686=i386)
ARCH=$(ARCH2:sparc64=sparcv9)
CFLAGS		+= $(CFLAGS/$(ARCH))
CFLAGS		+= -fPIC
OS		= linux
LIB_EXT		= .so
CC 		= riscv64-unknown-linux-gnu-gcc # FIXME: workaround only for riscv
CPP 	= riscv64-unknown-linux-gnu-g++ # FIXME: workaround only for riscv
endif
CFLAGS		+= -O
DLDFLAGS	+= -shared
LDFLAGS         += -ldl
OUTFLAGS	+= -o $@

LIBARCH		= $(ARCH)
ifdef		LP64
LIBARCH64/sparc	= sparcv9
LIBARCH64/i386	= amd64
LIBARCH64/riscv64	= riscv64
LIBARCH64	= $(LIBARCH64/$(ARCH))
ifneq		($(LIBARCH64),)
LIBARCH		= $(LIBARCH64)
endif   # LIBARCH64/$(ARCH)
endif   # LP64

JDKARCH=$(LIBARCH:i386=i586)

ifeq            ($(BINUTILS),)
# Pop all the way out of the workspace to look for binutils.
# ...You probably want to override this setting.
BINUTILSDIR	= $(shell cd build/binutils;pwd)
else
BINUTILSDIR	= $(shell cd $(BINUTILS);pwd)
endif

CPPFLAGS	+= -I$(BINUTILSDIR)/include -I$(BINUTILSDIR)/bfd -I$(TARGET_DIR)/bfd
CPPFLAGS	+= -DLIBARCH_$(LIBARCH) -DLIBARCH=\"$(LIBARCH)\" -DLIB_EXT=\"$(LIB_EXT)\"

TARGET_DIR	= build/$(OS)-$(JDKARCH)
TARGET		= $(TARGET_DIR)/hsdis-$(LIBARCH)$(LIB_EXT)

SOURCE		= hsdis.c

LIBRARIES =	$(TARGET_DIR)/bfd/libbfd.a \
		$(TARGET_DIR)/opcodes/libopcodes.a \
		$(TARGET_DIR)/libiberty/libiberty.a

DEMO_TARGET	= $(TARGET_DIR)/hsdis-demo
DEMO_SOURCE	= hsdis-demo.c

DISASSEMBLER_TEST	= $(TARGET_DIR)/disassembler-test
DISASSEMBLER_TEST_SOURCE	= disassembler_test.cpp

.PHONY:  all clean demo both

all:  $(TARGET)

both: all all64

%64:
	$(MAKE) LP64=1 ${@:%64=%}

demo: $(TARGET) $(DEMO_TARGET)

disassembler-test: $(TARGET) $(DISASSEMBLER_TEST)
	@echo -e "\n\n\n[Finish] finish building \$\(disassembler-test\)!\n\n"

$(LIBRARIES): $(TARGET_DIR) $(TARGET_DIR)/Makefile
	cd $(TARGET_DIR); make all-opcodes;

$(TARGET_DIR)/Makefile:
	(cd $(TARGET_DIR); CC=$(CC) CFLAGS="$(CFLAGS)" AR="$(AR)" $(BINUTILSDIR)/configure --host=riscv64 --disable-nls $(CONFIGURE_ARGS))

$(TARGET): $(SOURCE) $(LIBS) $(LIBRARIES) $(TARGET_DIR)
	$(CC) $(OUTFLAGS) $(CPPFLAGS) $(CFLAGS) $(SOURCE) $(DLDFLAGS) $(LIBRARIES)

$(DEMO_TARGET): $(DEMO_SOURCE) $(TARGET) $(TARGET_DIR)
	$(CC) $(OUTFLAGS) -DTARGET_DIR=\"$(TARGET_DIR)\" $(CPPFLAGS) -g $(CFLAGS/$(ARCH)) $(DEMO_SOURCE) $(LDFLAGS)

$(DISASSEMBLER_TEST): $(DISASSEMBLER_TEST_SOURCE) $(TARGET) $(TARGET_DIR)
	$(CPP) $(OUTFLAGS) -DTARGET_DIR=\"$(TARGET_DIR)\" $(CPPFLAGS) -O3 -std=c++14 -lpthread -g $(CFLAGS/$(ARCH)) $(DISASSEMBLER_TEST_SOURCE) $(LDFLAGS)

$(TARGET_DIR):
	[ -d $@ ] || mkdir -p $@

clean:
	rm -rf $(TARGET_DIR)
