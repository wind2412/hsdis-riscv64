//
// Created by wind2412 on 2022/5/9.
//

#undef __APPLE__
#ifndef __linux__    // I'm developing on Mac.
#define __linux__
#endif

#include "driver_tests.hpp"
#include <thread>
#include <utility>

#define __ masm.

class RealRun {
public:

  // disassembler test
  static void disassembler_test() {
    int code_size = DEFAULT_CODE_SIZE;
    address addr = DisassemblerTest::test([](Assembler& masm) {
        __ li(x10, 2);
        __ ret();

        // test self-made instructions
        __ c_lwu(x8, x9, 12);
    }, code_size);
    Code::unload_jitted_code(addr, code_size);
  }

  // ----------------------------------------------------

  // real run test (on Qemu)
  static void test1() {
    TestRunner::test_driver(__FUNCTION__,
                [](Assembler& masm) {
                    __ li(x10, 2);
                    __ ret();
                },
                2);
  }

  static void test_movoop() {
    const uint64_t guard_value48 = 0x7FFFF8000000;  // an oop
    const uint64_t answer = (guard_value48 & 0xFFFFFFFFFFFF);  // low 48-bit
    TestRunner::test_driver(__FUNCTION__,
                            [](Assembler& masm) {
                                address start = __ pc();
                                {
                                  __ mv(a0, (address)guard_value48);   // answer      // (6)  // Note: this should be right.
                                  // insert a sanity check for get_target_of_movptr().
                                  address val_fork = get_target_of_movptr(start);   // Note: this should be right.
                                  if ((uint64_t)val_fork != guard_value48) {
                                    printf("[Failed get_target_of_movptr] 0x%llx " INTPTR_FORMAT "\n", guard_value48, p2i(val_fork));
                                    fflush(stdout);
                                  }
                                  // insert a sanity check for patch_addr_offset()
                                  patch_addr_in_movptr(start, (address)guard_value48);    // Note: Might be buggy ???
                                  val_fork = get_target_of_movptr(start);
                                  if ((uint64_t)val_fork != guard_value48) {
                                    printf("[Failed patch_addr_in_movptr] 0x%llx " INTPTR_FORMAT "\n", guard_value48, p2i(val_fork));
                                    fflush(stdout);
                                  }
                                }

                                __ set_pc(start);

                                __ mv(a0, (address)guard_value48);
                                __ ret();
                            },
                            answer);
  }

  static void test_v8_liptr() {
    const uint64_t guard_value48 = 0x7FFFF8000000;  // an oop
    const uint64_t answer = (guard_value48 & 0xFFFFFFFFFFFF);  // low 48-bit
    TestRunner::test_driver(__FUNCTION__,
                            [](Assembler& masm) {
                                __ li_ptr(a0, guard_value48);
                                __ ret();
                            },
                            answer);
  }

  static void test_v8_movptr2_liptr_minus1() {
    const uint64_t guard_value64 = 0xFFFFFFFFFFFFFFFF;  // an 64-bit value
    const uint64_t answer = (guard_value64 & 0xFFFFFFFFFFFFFFFF);  // 64-bit
    TestRunner::test_driver(__FUNCTION__,
                            [](Assembler& masm) {
                                __ li_ptr(a0, guard_value64);  // cannot support -1...
                                __ c_nop();
                                __ c_nop();
                                __ c_nop();
                                __ c_nop();
                                __ movptr2(a0, (address)guard_value64);
                                __ ret();
                            },
                            answer);
  }

  static void test_movoop_li32() {
    const uint32_t guard_value32 = 0x7FFFF800;  // an oop: [0xFFFF_FFFF_8000_0000, 0x7FFF_FFFF]
    const uint32_t answer = (guard_value32 & 0xFFFFFFFF);  // low 32-bit
    TestRunner::test_driver(__FUNCTION__,
                            [](Assembler& masm) {
                                address start = __ pc();
                                {
                                  __ li32(a0, guard_value32);   // answer      // (6)  // Note: this should be right.
                                  // insert a sanity check for get_target_of_movptr().
                                  address val_fork = get_target_of_li32(start);   // Note: this should be right.
                                  if (val_fork != (address)guard_value32) {
                                    printf("[Failed get_target_of_movptr] 0x%llx " INTPTR_FORMAT "\n", /* using %llx will sign-extend guard_value32 -> 64bit by default. so must manually use (uint64_t). */ (uint64_t)guard_value32, p2i(val_fork));
                                    fflush(stdout);
                                  }
                                  // insert a sanity check for patch_addr_offset()
                                  patch_imm_in_li32(start, guard_value32);    // Note: Might be buggy ???
                                  val_fork = get_target_of_li32(start);
                                  if (val_fork != (address)guard_value32) {
                                    printf("[Failed patch_addr_in_movptr] 0x%llx " INTPTR_FORMAT "\n", (uint64_t)guard_value32, p2i(val_fork));
                                    fflush(stdout);
                                  }
                                }

                                __ set_pc(start);

                                __ li32(a0, guard_value32);
                                __ ret();
                            },
                            answer);
  }

  static void test_li64() {
    const uint64_t guard_value64 = 0xFFFFFFFFFFADBEEF;  // an 64-bit value
    const uint64_t answer = (guard_value64 & 0xFFFFFFFFFFFFFFFF);  // 64-bit
    TestRunner::test_driver(__FUNCTION__,
                            [](Assembler& masm) {
                                address start = __ pc();
                                {
                                  __ li64(a0, guard_value64);   // answer      // (6)  // Note: this should be right.
                                  // insert a sanity check for get_target_of_movptr().
                                  address val_fork = get_target_of_li64(start);   // Note: this should be right.
                                  if (val_fork != (address)guard_value64) {
                                    printf("[Failed get_target_of_movptr] 0x%llx " INTPTR_FORMAT "\n", /* using %llx will sign-extend guard_value32 -> 64bit by default. so must manually use (uint64_t). */ (uint64_t)guard_value64, p2i(val_fork));
                                    fflush(stdout);
                                  }
                                  // insert a sanity check for patch_addr_offset()
                                  patch_imm_in_li64(start, (address)guard_value64);    // Note: Might be buggy ???
                                  val_fork = get_target_of_li64(start);
                                  if (val_fork != (address)guard_value64) {
                                    printf("[Failed patch_addr_in_movptr] 0x%llx " INTPTR_FORMAT "\n", (uint64_t)guard_value64, p2i(val_fork));
                                    fflush(stdout);
                                  }
                                }

                                __ set_pc(start);

                                __ li64(a0, guard_value64);
                                __ ret();
                            },
                            answer);
  }

  static void test_mov_minus1() {
    const uint64_t guard_value64 = 0xFFFFFFFFFFFFFFFF;  // -1
    TestRunner::test_driver(__FUNCTION__,
                            [](Assembler& masm) {
                                address start = __ pc();
                                {
                                  __ mv(a0, (address)guard_value64);   // answer
                                  // insert a sanity check for get_target_of_movptr().
                                  address val_fork = get_target_of_movptr(start);   // Note: this should be right.
                                  if ((uint64_t)val_fork != guard_value64) {
                                    printf("[Failed get_target_of_movptr] 0x%llx " INTPTR_FORMAT "\n", guard_value64, p2i(val_fork));
                                    fflush(stdout);
                                  }
                                  // insert a sanity check for patch_addr_offset()
                                  patch_addr_in_movptr(start, (address)guard_value64);    // Note: Might be buggy ???
                                  val_fork = get_target_of_movptr(start);
                                  if ((uint64_t)val_fork != guard_value64) {
                                    printf("[Failed patch_addr_in_movptr] 0x%llx " INTPTR_FORMAT "\n", guard_value64, p2i(val_fork));
                                    fflush(stdout);
                                  }
                                }

                                __ set_pc(start);

                                __ mv(a0, (address)guard_value64);
                                __ ret();
                            },
                            guard_value64);
  }

  static void test_sanity_li() {
    const int code_size = DEFAULT_CODE_SIZE;
    for (uint64_t val = 0; val <= 0x7FFFFFFFFFFF; val++) {   // TODO: movptr_with_offset's val is 0~47. I think it should be changed...
      const uint64_t answer = (((uint64_t)val) & 0xFFFFFFFFFFFF);  // low 48-bit
      assert(((uint64_t)val) == answer && "sanity");   // the check is dummy of course. bits are definitely not changed.
      TestRunner::test_driver(__FUNCTION__,
                              [&](Assembler& masm) {
                                  __ li(a0, val);
                                  __ ret();
                              },
                              answer,
                              code_size,
                              0,
                              false);
    }
  }

  static void print_all(uint64_t reg1, uint64_t reg2) {
    printf("[Fatal] Difference detected: 0x%llx 0x%llx", reg1, reg2);
    fflush(stdout);
  }

#define REVERSE 1

  static void test_sanity_movptr() {
    const uint64_t code_size = 10000000;
    const uint64_t codeslice_size = 23 * 4;
    const uint64_t sampling_stride = 10000;
    const uint64_t stride = code_size / codeslice_size;

    auto && code_gen = [](Assembler &masm, uint64_t val) {
      const uint64_t answer = val & 0xFFFFFFFFFFFF;  // low 48-bit
      assert(val == answer && "sanity");   // the check is dummy of course. bits are definitely not changed.

      address start = __ pc();

      Label succeeded;

      {
        __ movptr2(a0, (address)val);   // answer      // (6)  // Note: this should be right.
        // insert a sanity check for get_target_of_movptr().
        address val_fork = get_target_of_movptr2(start);   // Note: this should be right.
        if ((uint64_t)val_fork != val) {
          printf("[Failed get_target_of_movptr] 0x%llx " INTPTR_FORMAT "\n", val, p2i(val_fork));
          fflush(stdout);
        }
        // insert a sanity check for patch_addr_offset()
        patch_addr_in_movptr2(start, (address)val);    // Note: Might be buggy ???
        val_fork = get_target_of_movptr2(start);
        if ((uint64_t)val_fork != val) {
          printf("[Failed patch_addr_in_movptr] 0x%llx " INTPTR_FORMAT "\n", val, p2i(val_fork));
          fflush(stdout);
        }
      }

      __ set_pc(start);

      __ movptr2(a0, (address)val);   // answer      // (6)
      __ li(a1, val);            // expected    // (<= 8)
      __ beq(a0, a1, succeeded);             // (1)
      {
        // failed
        __ call((address)RealRun::print_all);      // (7)
      }
      __ bind(succeeded);
    };
    auto && run_code_and_unload = [](address mm) {
      Code::install_jitted_code(__FUNCTION__, mm, code_size, RED_PRINT, false);

      reinterpret_cast<func>(mm)   ();   // call this.

      fflush(stdout);
      Code::unload_jitted_code(mm, code_size);
    };
    auto && main = [&code_gen, &run_code_and_unload](uint64_t start, uint64_t end) {
      clock_t timer1 = clock(), timer2;

      // [0, 0x57f00000] has been tested; [0x70FFFFFFFFFF, 0x710041600000] also
      uint64_t unit = 0x100000UL;
#if REVERSE
      uint64_t counter = end / unit + 1;
      for (; end >= start; end -= stride * sampling_stride) {   // TODO: movptr_with_offset's val is 0~47. I think it should be changed...
        if (end <= unit * counter) {
#else
      uint64_t counter = start / unit;
      for (; start <= end; start += stride) {   // TODO: movptr_with_offset's val is 0~47. I think it should be changed...
        if (start >= unit * counter) {
#endif
          // timer mode
          timer2 = clock();
          printf("Time mode:  Running [%d] ops costs: %s[%lf]%ss\n", stride, GREEN_PRINT, (double)(timer2-timer1)/CLOCKS_PER_SEC, RESET_COLOR);
          timer1 = timer2;
          // every X times print one message to tell us it's alive
#if REVERSE
          printf("[Testing] [0x%llx]\n", end);
          counter--;
#else
          printf("[Testing] [0x%llx]\n", start);
          counter++;
#endif
          fflush(stdout);
        }
        // manually loop opt
        address mm = Code::mmap_blob(code_size);
        Assembler masm(mm, code_size);
#if REVERSE
        for (uint64_t i = 0; i < stride && (start + i) <= end; i++) {
          code_gen(masm, end - i * sampling_stride);
        }
#else
        for (uint64_t i = 0; i < stride && (start + i) <= end; i++) {
          code_gen(masm, start + i * sampling_stride);
        }
#endif
        __ ret();
        run_code_and_unload(mm);
      }
    };

#define MAX_THREAD_NUM  50
    const uint64_t start = 0x0000007FFFFFFFF0; // TODO: change to 0
    const uint64_t end   = 0x00007FFFFFFFFFFF;
    const uint64_t range = (end - start) / MAX_THREAD_NUM + 1;
    // divide
    uint64_t tmp = start;
    std::pair<uint64_t, uint64_t> pairs[MAX_THREAD_NUM];
    for (int i = 0; i < MAX_THREAD_NUM; i++) {
      uint64_t next = std::min(tmp + range, end);
      pairs[i] = std::make_pair(tmp, next);
      tmp = next;
    }

    std::thread threads[MAX_THREAD_NUM];
    for (int i = 0; i < MAX_THREAD_NUM; i++) {
      threads[i] = std::thread(main, pairs[i].first, pairs[i].second);
    }

    for (int i = 0; i < MAX_THREAD_NUM; i++) {
      threads[i].join();
    }

    printf("[Test] finished\n");
    fflush(stdout);
  }

  static void test_c_lwu() {
    const unsigned long guard_value = 0xFFFFFFFFFEADBEEF;
    const unsigned long answer = (guard_value & 0xFFFFFFFF);  // low 32-bit
    TestRunner::test_driver(__FUNCTION__,
                            [](Assembler& masm) {
                                __ push_reg_excluding(RegSet::of(a0), sp);
                                {
                                  __ subi(sp, sp, 8);  // a 64-bit slot
                                  // push the guard value onto the stack
                                  __ li(t0, guard_value);   // a 64-bit guard value
                                  __ sd(t0, sp, 0);
                                  // then load it (use x8 && x9 because compressed regs are x8~x15)
                                  __ mv(x9, sp);
                                  __ c_lwu(x8, x9, 0);  // equals to: lwu(t0, sp, 0)
                                  __ mv(a0, x8);  // result is in a0
                                  __ addi(sp, sp, 8);  // restore sp
                                }
                                __ pop_reg_excluding(RegSet::of(a0), sp);
                                __ ret();
                            },
                            answer);
  }

  static void test_real_li48() {
    {
      const int64_t guard_value48 = 0x7FFFFEADBEEF;
      const int64_t answer = ((guard_value48 & 0xFFFFFFFFFFFF) << 16) >> 16;  // low 48-bit with sign extension
      TestRunner::test_driver(__FUNCTION__,
                              [](Assembler& masm) {
                                  __ real_li48(a0, guard_value48);   // a 48-bit guard value
                                  __ ret();
                              },
                              answer);
    } {
      const int64_t guard_value48 = (int64_t)-1;
      const int64_t answer = (int64_t)-1;
      TestRunner::test_driver(__FUNCTION__,
                              [](Assembler& masm) {
                                  __ real_li48(a0, guard_value48);   // a 48-bit guard value
                                  __ ret();
                              },
                              answer);
    }
  }

  static void test_real_jal48() {
    const int64_t guard_value48 = (((int64_t)0xFFFFFEADBEEF) << 16) >> 16;  // sign extended
    const int64_t answer = (((int64_t)(guard_value48 & 0xFFFFFFFFFFFF) << 16)) >> 16;  // low 48-bit with signed extension
    TestRunner::test_driver(__FUNCTION__,
                            [](Assembler& masm) {
                                Label trampoline;
                                __ enter();
                                __ real_jal48(ra, trampoline);
                                __ ebreak();  // a bomb: we would directly go to the next instruction.
                                __ leave();

                                Label target;
                                __ real_jal48(zr, target);
                                __ align(wordSize);   // alignment, for fun
                                __ ebreak();   // a bomb: Should not reach here
                                __ bind(target);
                                __ real_li48(a0, guard_value48);  // load an imm as the answer to test
                                __ ret();

                                __ bind(trampoline);
                                __ addi(ra, ra, 4);
                                __ ret();
                            },
                            answer);
  }

};

int main(int argc, char *argv[])
{
  RealRun::disassembler_test();

  RealRun::test1();
  // RealRun::test_movoop();
  // RealRun::test_v8_liptr();
  // RealRun::test_v8_movptr2_liptr_minus1();
  // RealRun::test_mov_minus1();
  // RealRun::test_movoop_li32();  // these are for testing #PR 8913 (https://github.com/openjdk/jdk/pull/8913). Now not needed.
  // RealRun::test_li64();

  // sanity tests
  // RealRun::test_sanity_li();
  // RealRun::test_sanity_movptr();

  RealRun::test_c_lwu();
  RealRun::test_real_li48();
  RealRun::test_real_jal48();
}