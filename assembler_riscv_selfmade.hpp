//
// Created by wind2412 on 2022/5/9.
//

#ifndef HSDIS_RISCV64_ASSEMBLER_RISCV_SELFMADE_HPP
#define HSDIS_RISCV64_ASSEMBLER_RISCV_SELFMADE_HPP

// c.lwu
#define INSN(NAME, funct3, op)                                                               \
  void NAME(Register Rd_Rs2, Register Rs1, uint32_t uimm) {                                  \
    assert_cond(is_unsigned_imm_in_range(uimm, 7, 0));                                       \
    assert_cond((uimm & 0b11) == 0);                                                         \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch_compressed_reg((address)&insn, 2, Rd_Rs2);                                       \
    c_patch((address)&insn, 5, 5, (uimm & nth_bit(6)) >> 6);                                 \
    c_patch((address)&insn, 6, 6, (uimm & nth_bit(2)) >> 2);                                 \
    c_patch_compressed_reg((address)&insn, 7, Rs1);                                          \
    c_patch((address)&insn, 12, 10, (uimm & right_n_bits(6)) >> 3);                          \
    c_patch((address)&insn, 15, 13, funct3);                                                 \
    emit_int16(insn);                                                                        \
  }

  INSN(c_lwu, 0b100, 0b00);

#undef INSN

static void patch64(address a, unsigned msb, unsigned lsb, uint64_t val) {
  assert(a != NULL);
  assert(msb >= lsb && msb <= 63);
  unsigned nbits = msb - lsb + 1;
  assert(val < (1UL << nbits) && "Field too big for insn");
  unsigned mask = (1UL << nbits) - 1;
  val <<= lsb;
  mask <<= lsb;
  uint64_t target = *(uint64_t *)a;
  target &= ~mask;
  target |= val;
  *(uint64_t *)a = target;
}

static void patch64_reg(address a, unsigned lsb, Register reg) {
  patch64(a, lsb + 4, lsb, reg->encoding_nocheck());
}

#define INSN(NAME, op4, op7)                                                                \
  void NAME(Register Rd, int64_t imm) {                                                     \
    /* signed li48 is in range 0x8000 0000 0000 ~ 0x7fff ffff ffff,
       and a special 0xffff ffff ffff ffff    */                                            \
    assert((is_imm_in_range(imm, 48, 0) || imm == -1) && "imm is out of validity");         \
    uint64_t insn = 0;                                                                      \
    patch64((address)&insn, 6, 0, op7);                                                     \
    patch64((address)&insn, 10, 7, op4);                                                    \
    patch64_reg((address)&insn, 11, Rd);                                                    \
    patch64((address)&insn, 63, 16, (imm & 0xffffffffffff));                                \
    emit_int64(insn);                                                                       \
  }

  INSN(real_li48,  0b0000, 0b0111111);
#undef INSN

#define INSN(NAME, op4, op7)                                                                \
  void NAME(Register Rd, int64_t imm) {                                                     \
    assert(is_imm_in_range(imm, 48, 1) && "offset is invalid");                             \
    uint64_t insn = 0;                                                                      \
    patch64((address)&insn, 6, 0, op7);                                                     \
    patch64((address)&insn, 10, 7, op4);                                                    \
    patch64_reg((address)&insn, 11, Rd);                                                    \
    patch64((address)&insn, 62, 16, (imm & 0xffffffffffff) >> 1);                           \
    emit_int64(insn);                                                                       \
  }                                                                                         \
                                                                                            \
  void NAME(Register Rd, const address dest) {                                              \
    assert(dest != NULL);                                                                   \
    int64_t offset = dest - _end;                                                           \
    if (is_imm_in_range(offset, 48, 1)) {                                                   \
      NAME(Rd, offset);                                                                     \
    } else {                                                                                \
      assert(false);                                                                        \
    }                                                                                       \
  }                                                                                         \
                                                                                            \
  void NAME(Register Rd, Label &L) {                                                        \
    do_label(Rd, L, &Assembler::NAME);                                                      \
  }

  INSN(real_jal48, 0b0001, 0b0111111);
#undef INSN


#endif //HSDIS_RISCV64_ASSEMBLER_RISCV_SELFMADE_HPP
