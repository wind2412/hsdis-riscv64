//
// Created by wind2412 on 2022/5/9.
//

#ifndef HSDIS_RISCV64_CODEBLOB_HPP
#define HSDIS_RISCV64_CODEBLOB_HPP

#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include "assembler_utils.hpp"
#include "disassembler.hpp"
#include <iostream>

class Code {
public:
  constexpr static bool non_fixed = false;
public:
  static address mmap_blob(address hint, size_t code_size) {
    long decor = MAP_ANON | MAP_PRIVATE;
    if (!non_fixed) {
      decor |= MAP_FIXED;
    }
    address mm = (address)mmap(hint, code_size, PROT_READ | PROT_WRITE, decor, -1, 0);
    if (mm == MAP_FAILED) {
      printf("Fatal: mmap failed. Exit -1. \n");
      exit(-1);
    }
    return mm;
  }
  static address mmap_blob(size_t code_size) {
    long decor = MAP_ANON | MAP_PRIVATE;
    address mm = (address)mmap(NULL, code_size, PROT_READ | PROT_WRITE, decor, -1, 0);
    if (mm == MAP_FAILED) {
      printf("Fatal: mmap failed. Exit -1. \n");
      fflush(stdout);
      exit(-1);
    }
    return mm;
  }

  static address install_jitted_code(const char *name, unsigned char *code, size_t code_size, address mm, const char *name_color = RED_PRINT, bool verbose = true) {
    memcpy(mm, code, code_size);
    mprotect(mm, code_size, PROT_READ | PROT_EXEC);
    if (verbose) {
      Disassembler::decode(mm, code_size, name, name_color);
    }
    return mm;
  }

  static address install_jitted_code(const char *name, address mm, size_t code_size, const char *name_color = RED_PRINT, bool verbose = true) {
    mprotect(mm, code_size, PROT_READ | PROT_EXEC);
    if (verbose) {
      Disassembler::decode(mm, code_size, name, name_color);
    }
    return mm;
  }

  static void unload_jitted_code(address mmap_addr, size_t code_size) {
    int ret = munmap(mmap_addr, code_size);
    if (ret == -1) {
      perror("mnumap");
      exit(-1);
    }
  }
};

#endif //HSDIS_RISCV64_CODEBLOB_HPP
