//
// Created by wind2412 on 2022/5/9.
//

#ifndef HSDIS_RISCV64_DRIVER_TESTS_HPP
#define HSDIS_RISCV64_DRIVER_TESTS_HPP

#include <time.h>
#include <math.h>
#include <functional>
#include "assembler_riscv.hpp"
#include "codeblob.hpp"
#include <iostream>

#define __ masm.

#define DEFAULT_CODE_SIZE    0x1000
#define DEFAULT_TEST_COUNT   1000

struct DisassemblerTest {
public:
  // disassembler sanity test; dry run but useful -- just check the instructions manually
  // you should mummap by yourself
  static address test(std::function<void(Assembler&)> codegen, int code_size = DEFAULT_CODE_SIZE) {
    address mm = Code::mmap_blob(code_size);
    Assembler masm(mm, code_size);

    codegen(masm);

    return Code::install_jitted_code("DisassemblerTest", mm, __ offset());
  }
};

struct TestRunner {
protected:
  // test if the answer is right
  static void sanity_test(func f, const char *name, uint64_t expect, bool verbose = true) {
    uint64_t ret = f();
    if (verbose) {
      printf("[Sanity Test] [%s] returns [0x%llx], and expecting [0x%llx]\n", name, ret, expect);
      fflush(stdout);
    }
    assert(ret == expect);
  }

  // a while-loop functionality test
  static void test(func f, int count = DEFAULT_TEST_COUNT) {
    for (int i = 0; i < count; i++) {
      f();
    }
  }

  // a perf test (under Qemu, it is not precise of course)
  static void test_perf(func f, const char *name, int count = DEFAULT_TEST_COUNT) {
    if (count == 0) {
      return;
    }

    printf("--------------   TESTING %s[%s]%s  ---------------\n", GREEN_PRINT, name, RESET_COLOR);
    clock_t timer1, timer2;
    timer1 = clock(); {
      test(f, count);
    } timer2 = clock();
    // printf("Thpt mode:  %lf op/ns\n", TEST_CNT_CONST/(double)(timer2-timer1));
    printf("Time mode:  Running [%d] ops costs: %s[%lf]%ss\n", count, GREEN_PRINT, (double)(timer2-timer1)/CLOCKS_PER_SEC, RESET_COLOR);
  }

public:
  static void test_driver(const char *name,
                          std::function<void(Assembler&)> codegen,
                          uint64_t answer = 0xcafecafecafecafeUL,
                          int code_size = DEFAULT_CODE_SIZE,
                          int test_count = DEFAULT_TEST_COUNT,
                          bool verbose = true
  ) {
    auto && code = [&](const char *name) {
      address mm = Code::mmap_blob(/*(address)0x7FFFF8000000, */ code_size);
      Assembler masm(mm, code_size);

      codegen(masm);

      return Code::install_jitted_code(name, mm, __ offset(), RED_PRINT, verbose);
    };

    address addr = code(name);
    func f = reinterpret_cast<func> (addr);

    if (answer != 0xcafecafecafecafe) {  // workaround: if answer is 0xcafecafecafecafe then no sanity test.
      sanity_test(f, name, answer, verbose);
    }
    test_perf(f, name, test_count);

    fflush(stdout);   // don't remove this! docker interactive mode sometimes needs this to get the right output when SIGSEGV happens (if we don't flush, sometimes no output).

    Code::unload_jitted_code(addr, code_size);
  }

};

#endif //HSDIS_RISCV64_DRIVER_TESTS_HPP
