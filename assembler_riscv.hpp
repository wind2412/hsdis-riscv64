//
// Created by wind2412 on 2022/5/9.
//

#ifndef HSDIS_RISCV64_ASSEMBLER_RISCV_HPP
#define HSDIS_RISCV64_ASSEMBLER_RISCV_HPP

#include "assembler_utils.hpp"

bool UseLSE = false;
bool UsePrimitive = false;

class Label {
private:
  address _patch_address = NULL;
  address _target_address = NULL;
public:
  bool is_bound() const { return _target_address != NULL; }

  address patch_address() const { return _patch_address; }
  void set_patch_address(address addr) { _patch_address = addr; }

  address target_address() const { return _target_address; }
  void set_target_address(address addr) { _target_address = addr; }
};

class Assembler {
private:
  address _start;
  address _end;
  address _bottom;
public:
  Assembler(address start, unsigned size) : _start(start), _end(start), _bottom(_end + size) {}
public:
  unsigned offset() { return _end - _start; }
  address pc() { return _end; }
  void set_pc(address end) {
    assert(end < _bottom && "sanity"); _end = end;
  }
public:
  static bool is_imm_in_range(long value, unsigned bits, unsigned align_bits) {
    intptr_t sign_bits = (value >> (bits + align_bits - 1));
    return ((value & right_n_bits(align_bits)) == 0) && ((sign_bits == 0) || (sign_bits == -1));
  }

  static bool is_unsigned_imm_in_range(intptr_t value, unsigned bits, unsigned align_bits) {
    return (value >= 0) && ((value & right_n_bits(align_bits)) == 0) && ((value >> (align_bits + bits)) == 0);
  }

  static bool is_offset_in_range(intptr_t offset, unsigned bits) {
    return is_imm_in_range(offset, bits, 0);
  }

  static void patch(address a, unsigned msb, unsigned lsb, unsigned val) {
    assert(a != NULL);
    assert(msb >= lsb && msb <= 31);
    unsigned nbits = msb - lsb + 1;
    assert(val < (1U << nbits) && "Field too big for insn");
    unsigned mask = (1U << nbits) - 1;
    val <<= lsb;
    mask <<= lsb;
    unsigned target = *(unsigned *)a;
    target &= ~mask;
    target |= val;
    *(unsigned *)a = target;
  }

  static void patch(address a, unsigned bit, unsigned val) {
    patch(a, bit, bit, val);
  }

  static void patch_reg(address a, unsigned lsb, Register reg) {
    patch(a, lsb + 4, lsb, reg->encoding_nocheck());
  }

  static uint32_t extract(uint32_t val, unsigned msb, unsigned lsb) {
    assert(msb >= lsb && msb <= 31);
    unsigned nbits = msb - lsb + 1;
    uint32_t mask = (1U << nbits) - 1;
    uint32_t result = val >> lsb;
    result &= mask;
    return result;
  }

  static int32_t sextract(uint32_t val, unsigned msb, unsigned lsb) {
    assert(msb >= lsb && msb <= 31);
    int32_t result = val << (31 - msb);
    result >>= (31 - msb + lsb);
    return result;
  }

public:
  void emit_int8(int8_t x) {
    *((int8_t*) pc()) = x;
    set_pc(pc() + sizeof(int8_t));
  }
  void emit_int16(int16_t x) {
    *((int16_t*) pc()) = x;
    set_pc(pc() + sizeof(int16_t));
  }
  void emit_int32(int32_t x) {
    *((int32_t*) pc()) = x;
    set_pc(pc() + sizeof(int32_t));
  }
  void emit_int64(int64_t x) {
    *((int64_t*) pc()) = x;
    set_pc(pc() + sizeof(int64_t));
  }

public:
  // label things
  void bind(Label & l) {
    assert(!l.is_bound() && "bind only once");
    address patch_pc = l.patch_address();
    if (patch_pc) {              // have had a jump and now bind
      patch_instruction(patch_pc, _end);
    } else {                        // just a raw bind now.
      l.set_target_address(_end);
    }
  }

  // align things
  void align(int modulus) {
    while (offset() % modulus != 0) { nop(); }
  }

public:
  // rv
  // Immediate Instruction
#define INSN(NAME, op, funct3, compress)                                                    \
  void NAME(Register Rd, Register Rs1, int32_t imm) {                                       \
    assert(is_imm_in_range(imm, 12, 0) && "Immediate is out of validity");                  \
    unsigned insn = 0;                                                                      \
    patch((address)&insn, 6, 0, op);                                                        \
    patch((address)&insn, 14, 12, funct3);                                                  \
    patch((address)&insn, 31, 20, imm & 0x00000fff);                                        \
    patch_reg((address)&insn, 7, Rd);                                                       \
    patch_reg((address)&insn, 15, Rs1);                                                     \
    emit_int32(insn);                                                                       \
  }
    INSN(addi,  0b0010011, 0b000, true);
    INSN(slti,  0b0010011, 0b010, false);
    INSN(addiw, 0b0011011, 0b000, true);
    INSN(and_imm12,  0b0010011, 0b111, true);
    INSN(ori,   0b0010011, 0b110, false);
    INSN(xori,  0b0010011, 0b100, false);
#undef INSN

    void subi(Register Rd, Register Rs1, int32_t imm) {
      addi(Rd, Rs1, -imm);
    }

    void neg(Register Rd, Register Rs) {
      sub(Rd, x0, Rs);
    }

    void nop() {
      addi(x0, x0, 0);
    }

    // Shift Immediate Instruction
#define INSN(NAME, op, funct3, funct6)                                   \
  void NAME(Register Rd, Register Rs1, unsigned shamt) {                 \
    assert(shamt <= 0x3f && "Shamt is invalid");                         \
    unsigned insn = 0;                                                   \
    patch((address)&insn, 6, 0, op);                                     \
    patch((address)&insn, 14, 12, funct3);                               \
    patch((address)&insn, 25, 20, shamt);                                \
    patch((address)&insn, 31, 26, funct6);                               \
    patch_reg((address)&insn, 7, Rd);                                    \
    patch_reg((address)&insn, 15, Rs1);                                  \
    emit_int32(insn);                                                    \
  }

    INSN(slli,  0b0010011, 0b001, 0b000000);
    INSN(srai,  0b0010011, 0b101, 0b010000);
    INSN(srli,  0b0010011, 0b101, 0b000000);

#undef INSN

    void li_helper(Register Rd, int64_t imm) {
      // int64_t is in range 0x8000 0000 0000 0000 ~ 0x7fff ffff ffff ffff
      int shift = 12;
      int64_t upper = imm, lower = imm;
      // Split imm to a lower 12-bit sign-extended part and the remainder, because addi will sign-extend the lower imm.
      lower = ((int32_t)imm << 20) >> 20;
      upper -= lower;

      // Test whether imm is a 32-bit integer.
      if (!(((imm) & ~(int64_t)0x7fffffff) == 0 ||
            (((imm) & ~(int64_t)0x7fffffff) == ~(int64_t)0x7fffffff))) {
        while (((upper >> shift) & 1) == 0) { shift++; }
        upper >>= shift;
        li_helper(Rd, upper);
        slli(Rd, Rd, shift);
        if (lower != 0) {
          addi(Rd, Rd, lower);
        }
      }
      else {
        // 32-bit integer
        Register hi_Rd = zr;
        if (upper != 0) {
          lui(Rd, (int32_t)upper);
          hi_Rd = Rd;
        }
        if (lower != 0 || hi_Rd == zr) {
          addiw(Rd, hi_Rd, lower);
        }
      }
    }

    void li(Register Rd, int64_t imm) {
      li_helper(Rd, imm);
    }

    // Load/store register (all modes)
#define INSN(NAME, op, funct3, compress)                                                           \
  void NAME(Register Rd, Register Rs, const int32_t offset) {                                      \
    unsigned insn = 0;                                                                             \
    assert(is_offset_in_range(offset, 12) && "offset is invalid.");                                \
    int32_t val = offset & 0xfff;                                                                  \
    patch((address)&insn, 6, 0, op);                                                               \
    patch((address)&insn, 14, 12, funct3);                                                         \
    patch_reg((address)&insn, 15, Rs);                                                             \
    patch_reg((address)&insn, 7, Rd);                                                              \
    patch((address)&insn, 31, 20, val);                                                            \
    emit_int32(insn);                                                                              \
  }                                                                                                \
  void NAME(Register Rd, address dest) {                                                           \
    assert(dest != NULL);                                                                          \
    int64_t distance = (dest - _end);                                                              \
    if (is_offset_in_range(distance, 32)) {                                                        \
      auipc(Rd, (int32_t)distance + 0x800);                                                        \
      NAME(Rd, Rd, ((int32_t)distance << 20) >> 20);                                               \
    } else {                                                                                       \
      assert(false);                                                                               \
    }                                                                                              \
  }                                                                                                \
  void NAME(Register Rd, Label &L) {                                                               \
    do_label(Rd, L, &Assembler::NAME);                                                             \
  }

    INSN(lb,  0b0000011, 0b000, false);
    INSN(lbu, 0b0000011, 0b100, false);
    INSN(ld,  0b0000011, 0b011, true);
    INSN(lh,  0b0000011, 0b001, false);
    INSN(lhu, 0b0000011, 0b101, false);
    INSN(lw,  0b0000011, 0b010, true);
    INSN(lwu, 0b0000011, 0b110, false);
#undef INSN

#define INSN(NAME, REGISTER, op, funct3)                                                                    \
  void NAME(REGISTER Rs1, Register Rs2, const int32_t offset) {                                             \
    unsigned insn = 0;                                                                                      \
    assert(is_offset_in_range(offset, 12) && "offset is invalid.");                                         \
    uint32_t val  = offset & 0xfff;                                                                         \
    uint32_t low  = val & 0x1f;                                                                             \
    uint32_t high = (val >> 5) & 0x7f;                                                                      \
    patch((address)&insn, 6, 0, op);                                                                        \
    patch((address)&insn, 14, 12, funct3);                                                                  \
    patch_reg((address)&insn, 15, Rs2);                                                                     \
    patch_reg((address)&insn, 20, Rs1);                                                                     \
    patch((address)&insn, 11, 7, low);                                                                      \
    patch((address)&insn, 31, 25, high);                                                                    \
    emit_int32(insn);                                                                                       \
  }                                                                                                         \

    INSN(sb,  Register,      0b0100011, 0b000);
    INSN(sh,  Register,      0b0100011, 0b001);
    INSN(sw,  Register,      0b0100011, 0b010);
    INSN(sd,  Register,      0b0100011, 0b011);
#undef INSN

    void la_with_offset(Register reg1, address dest, int32_t &offset) {
      // I'm too lazy to gen lots of bound checking. let it go.
      int64_t distance = dest - _end;
      auipc(reg1, (int32_t)distance + 0x800);
      offset = ((int32_t)distance << 20) >> 20;
    }
    // Upper Immediate Instruction
#define INSN(NAME, op, compress)                                        \
  void NAME(Register Rd, int32_t imm) {                                 \
    int32_t upperImm = imm >> 12;                                       \
    unsigned insn = 0;                                                  \
    patch((address)&insn, 6, 0, op);                                    \
    patch_reg((address)&insn, 7, Rd);                                   \
    upperImm &= 0x000fffff;                                             \
    patch((address)&insn, 31, 12, upperImm);                            \
    emit_int32(insn);                                                   \
  }

    INSN(lui,    0b0110111, true);
    INSN(auipc,  0b0010111, false);
#undef INSN

#define INSN(NAME, op, funct, compress)                                                    \
  void NAME(Register Rd, Register Rs, const int32_t offset) {                              \
    unsigned insn = 0;                                                                     \
    assert(is_offset_in_range(offset, 12) && "offset is invalid.");                        \
    patch((address)&insn, 6, 0, op);                                                       \
    patch_reg((address)&insn, 7, Rd);                                                      \
    patch((address)&insn, 14, 12, funct);                                                  \
    patch_reg((address)&insn, 15, Rs);                                                     \
    int32_t val = offset & 0xfff;                                                          \
    patch((address)&insn, 31, 20, val);                                                    \
    emit_int32(insn);                                                                      \
  }
    INSN(jalr, 0b1100111, 0b000, true);
#undef INSN

    void ret() { jalr(zr, lr, 0); }

    void call(address entry) {
      if (true) {
        int32_t offset = 0;
        movptr_with_offset(t0, entry, offset);
        jalr(x1, t0, offset);
      } else {
        li(t0, (int64_t)entry);
        jalr(x1, t0, 0);
      }
    }

#define INSN(NAME, op, compress)                                                              \
  void NAME(Register Rd, const int32_t offset) {                                              \
    unsigned insn = 0;                                                                        \
    assert(is_imm_in_range(offset, 20, 1) && "offset is invalid.");                           \
    patch((address)&insn, 6, 0, op);                                                          \
    patch_reg((address)&insn, 7, Rd);                                                         \
    patch((address)&insn, 19, 12, (uint32_t)((offset >> 12) & 0xff));                         \
    patch((address)&insn, 20, (uint32_t)((offset >> 11) & 0x1));                              \
    patch((address)&insn, 30, 21, (uint32_t)((offset >> 1) & 0x3ff));                         \
    patch((address)&insn, 31, (uint32_t)((offset >> 20) & 0x1));                              \
    emit_int32(insn);                                                                         \
  }                                                                                           \
  void NAME(Register Rd, const address dest) {                                                \
    assert(dest != NULL);                                                                     \
    int64_t offset = dest - _end;                                                             \
    if (is_imm_in_range(offset, 20, 1)) {                                                     \
      NAME(Rd, offset);                                                                       \
    } else {                                                                                  \
      assert(false);                                                                          \
    }                                                                                         \
  }                                                                                           \
  void NAME(Register Rd, Label &L) {                                                          \
    do_label(Rd, L, &Assembler::NAME);                                                        \
  }

    INSN(jal, 0b1101111, true);
#undef INSN

#define INSN(NAME, REGISTER, compress)                             \
  void NAME(const address &dest) {                                 \
    assert(dest != NULL);                                          \
    int64_t distance = dest - _end;                                \
    if (is_imm_in_range(distance, 20, 1)) {                        \
      jal(REGISTER, distance);                                     \
    } else {                                                       \
      assert(false);                                               \
    }                                                              \
  }                                                                \
  void NAME(Label &l) {                                            \
    jal(REGISTER, l);                                              \
  }                                                                \

    INSN(j,    x0, true);
    INSN(jal,  x1, true);
#undef INSN

#define INSN(NAME, REGISTER, compress)                             \
  void NAME(Register Rs) {                                         \
    jalr(REGISTER, Rs, 0);                                         \
  }

    INSN(jr,      x0, true);
    INSN(jalr,    x1, true);

#undef INSN

    void patch_instruction(address branch, address target) {
      assert(branch != NULL);
      int64_t offset = target - branch;
      unsigned insn = *(unsigned*)branch;
      if (Assembler::extract(insn, 6, 0) == 0b1101111) {         // jal
        assert(is_imm_in_range(offset, 20, 1) && "offset is too large to be patched in one jal instruction!\n");
        Assembler::patch(branch, 31, 31, (offset >> 20) & 0x1);                       // offset[20]    ==> branch[31]
        Assembler::patch(branch, 30, 21, (offset >> 1)  & 0x3ff);                     // offset[10:1]  ==> branch[30:21]
        Assembler::patch(branch, 20, 20, (offset >> 11) & 0x1);                       // offset[11]    ==> branch[20]
        Assembler::patch(branch, 19, 12, (offset >> 12) & 0xff);                      // offset[19:12] ==> branch[19:12]
        return;
      } else if (Assembler::extract(insn, 6, 0) == 0b0010111) {          // auipc
        unsigned insn2 = *(unsigned*)(branch + 4);
        if (Assembler::extract(insn2, 6, 0) == 0b0000011 || // ld
            (Assembler::extract(insn2, 6, 0) == 0b0010011 && Assembler::extract(insn2, 14, 12) == 0b000) || // addi
            (Assembler::extract(insn2, 6, 0) == 0b1100111 && Assembler::extract(insn2, 14, 12) == 0b000)    // jalr
                ) {
          Assembler::patch(branch, 31, 12, ((offset + 0x800) >> 12) & 0xfffff);       // Auipc.          offset[31:12]  ==> branch[31:12]
          Assembler::patch(branch + 4, 31, 20, offset & 0xfff);                    // Addi/Jalr/Load. offset[11:0]   ==> branch[31:20]
          return;
        }
      } else if (Assembler::extract(insn, 6, 0) == 0b1100011) {  // beq/bge/bgeu/blt/bltu/bne
        assert(is_imm_in_range(offset, 12, 1) &&
               "offset is too large to be patched in one beq/bge/bgeu/blt/bltu/bne insrusction!\n");
        Assembler::patch(branch, 31, 31, (offset >> 12) & 0x1);                       // offset[12]    ==> branch[31]
        Assembler::patch(branch, 30, 25, (offset >> 5) & 0x3f);                       // offset[10:5]  ==> branch[30:25]
        Assembler::patch(branch, 7, 7, (offset >> 11) & 0x1);                         // offset[11]    ==> branch[7]
        Assembler::patch(branch, 11, 8, (offset >> 1) & 0xf);                         // offset[4:1]   ==> branch[11:8]
        return;
      } else if (Assembler::extract(insn, 6, 0) == 0b0111111 &&
              Assembler::extract(insn, 10, 7) == 0b0001) {
        assert(is_imm_in_range(offset, 48, 1) && "offset is too large to be patched in real.jal48 insrusction!\n");
        Assembler::patch64(branch, 62, 16, (offset & 0xffffffffffff) >> 0x1);
        return;
      }
      assert(false);
    }

    typedef void (Assembler::* load_branch_insn)(Register Rt, address dest);
    void do_label(Register Rt, Label &l, load_branch_insn insn) {
      if (l.is_bound()) {  // we have had a bind before and now this ldr
        (this->*insn)(Rt, l.target_address());
      } else {             // not binded at all
        l.set_patch_address(_end);
        (this->*insn)(Rt, _end);  // set to the current pc
      }
    }
    typedef void (Assembler::* uncond_branch_insn)(address dest);
    void do_label(Label &l, uncond_branch_insn insn) {
      if (l.is_bound()) {
        (this->*insn)(l.target_address());
      } else {
        l.set_patch_address(_end);
        (this->*insn)(_end);  // set to the curent pc
      }
    }

    typedef void (Assembler::* compare_and_branch_insn)(Register Rs1, Register Rs2, const address dest, bool compressed);
    typedef void (Assembler::* compare_and_branch_label_insn)(Register Rs1, Register Rs2, Label &L, bool is_far, bool compressed);

    void do_label(Register r1, Register r2, Label &L, compare_and_branch_insn insn,
                  compare_and_branch_label_insn neg_insn, bool is_far, bool compressed) {
      if (is_far) {
        Label done;
        (this->*neg_insn)(r1, r2, done, /* is_far */ false, true);
        j(L);
        bind(done);
      } else {
        if (L.is_bound()) {
          (this->*insn)(r1, r2, L.target_address(), compressed);
        } else {
          L.set_patch_address(pc());
          (this->*insn)(r1, r2, pc(), compressed);
        }
      }
    }

    enum operand_size { int8, int16, int32, uint32, int64 };
    enum Aqrl {relaxed = 0b00, rl = 0b01, aq = 0b10, aqrl = 0b11};

#define INSN(NAME, op, funct3, funct7)                                                  \
  void NAME(Register Rd, Register Rs1, Register Rs2, Aqrl memory_order = aqrl) {        \
    unsigned insn = 0;                                                                  \
    patch((address)&insn, 6, 0, op);                                                    \
    patch((address)&insn, 14, 12, funct3);                                              \
    patch_reg((address)&insn, 7, Rd);                                                   \
    patch_reg((address)&insn, 15, Rs1);                                                 \
    patch_reg((address)&insn, 20, Rs2);                                                 \
    patch((address)&insn, 31, 27, funct7);                                              \
    patch((address)&insn, 26, 25, memory_order);                                        \
    emit_int32(insn);                                                                   \
  }

    INSN(amoswap_w, 0b0101111, 0b010, 0b00001);
    INSN(amoadd_w,  0b0101111, 0b010, 0b00000);
    INSN(amoxor_w,  0b0101111, 0b010, 0b00100);
    INSN(amoand_w,  0b0101111, 0b010, 0b01100);
    INSN(amoor_w,   0b0101111, 0b010, 0b01000);
    INSN(amomin_w,  0b0101111, 0b010, 0b10000);
    INSN(amomax_w,  0b0101111, 0b010, 0b10100);
    INSN(amominu_w, 0b0101111, 0b010, 0b11000);
    INSN(amomaxu_w, 0b0101111, 0b010, 0b11100);
    INSN(amoswap_d, 0b0101111, 0b011, 0b00001);
    INSN(amoadd_d,  0b0101111, 0b011, 0b00000);
    INSN(amoxor_d,  0b0101111, 0b011, 0b00100);
    INSN(amoand_d,  0b0101111, 0b011, 0b01100);
    INSN(amoor_d,   0b0101111, 0b011, 0b01000);
    INSN(amomin_d,  0b0101111, 0b011, 0b10000);
    INSN(amomax_d , 0b0101111, 0b011, 0b10100);
    INSN(amominu_d, 0b0101111, 0b011, 0b11000);
    INSN(amomaxu_d, 0b0101111, 0b011, 0b11100);
#undef INSN

    // Rigster Instruction
#define INSN(NAME, op, funct3, funct7)                          \
  void NAME(Register Rd, Register Rs1, Register Rs2) {          \
    unsigned insn = 0;                                          \
    patch((address)&insn, 6,  0, op);                           \
    patch((address)&insn, 14, 12, funct3);                      \
    patch((address)&insn, 31, 25, funct7);                      \
    patch_reg((address)&insn, 7, Rd);                           \
    patch_reg((address)&insn, 15, Rs1);                         \
    patch_reg((address)&insn, 20, Rs2);                         \
    emit_int32(insn);                                           \
  }

    INSN(add,   0b0110011, 0b000, 0b0000000);
    INSN(sub,   0b0110011, 0b000, 0b0100000);
    INSN(andr,  0b0110011, 0b111, 0b0000000);
    INSN(orr,   0b0110011, 0b110, 0b0000000);
    INSN(xorr,  0b0110011, 0b100, 0b0000000);
    INSN(sll,   0b0110011, 0b001, 0b0000000);
    INSN(sra,   0b0110011, 0b101, 0b0100000);
    INSN(srl,   0b0110011, 0b101, 0b0000000);
    INSN(slt,   0b0110011, 0b010, 0b0000000);
    INSN(sltu,  0b0110011, 0b011, 0b0000000);
    INSN(addw,  0b0111011, 0b000, 0b0000000);
    INSN(subw,  0b0111011, 0b000, 0b0100000);
    INSN(sllw,  0b0111011, 0b001, 0b0000000);
    INSN(sraw,  0b0111011, 0b101, 0b0100000);
    INSN(srlw,  0b0111011, 0b101, 0b0000000);
    INSN(mul,   0b0110011, 0b000, 0b0000001);
    INSN(mulh,  0b0110011, 0b001, 0b0000001);
    INSN(mulhsu,0b0110011, 0b010, 0b0000001);
    INSN(mulhu, 0b0110011, 0b011, 0b0000001);
    INSN(mulw,  0b0111011, 0b000, 0b0000001);
    INSN(div,   0b0110011, 0b100, 0b0000001);
    INSN(divu,  0b0110011, 0b101, 0b0000001);
    INSN(divw,  0b0111011, 0b100, 0b0000001);
    INSN(divuw, 0b0111011, 0b101, 0b0000001);
    INSN(rem,   0b0110011, 0b110, 0b0000001);
    INSN(remu,  0b0110011, 0b111, 0b0000001);
    INSN(remw,  0b0111011, 0b110, 0b0000001);
    INSN(remuw, 0b0111011, 0b111, 0b0000001);

    // Vector Configuration Instruction
    INSN(vsetvl, 0b1010111, 0b111, 0b1000000);

#undef INSN

#define INSN(NAME, op, funct3, funct7)                      \
  void NAME() {                                             \
    unsigned insn = 0;                                      \
    patch((address)&insn, 6, 0, op);                        \
    patch((address)&insn, 11, 7, 0b00000);                  \
    patch((address)&insn, 14, 12, funct3);                  \
    patch((address)&insn, 19, 15, 0b00000);                 \
    patch((address)&insn, 31, 20, funct7);                  \
    emit_int32(insn);                                       \
  }

    INSN(fence_i, 0b0001111, 0b001, 0b000000000000);
    INSN(ecall,   0b1110011, 0b000, 0b000000000000);
    INSN(ebreak,  0b1110011, 0b000, 0b000000000001);
#undef INSN

    void andi(Register Rd, Register Rn, int64_t imm, Register tmp = t0) {
      if (is_imm_in_range(imm, 12, 0)) {
        and_imm12(Rd, Rn, imm);
      } else {
        li(tmp, imm);
        andr(Rd, Rn, tmp);
      }
    }

    void zero_ext(Register dst, Register src, int clear_bits) {
      slli(dst, src, clear_bits);
      srli(dst, dst, clear_bits);
    }

    void clear_upper_bits(Register r, unsigned upper_bits) {
      assert(upper_bits < 64 && "bit count to clear must be less than 64");

      int sig_bits = 64 - upper_bits; // significance bits
      if (sig_bits < 12) {
        andi(r, r, (1UL << sig_bits) - 1);
      } else {
        zero_ext(r, r, upper_bits);
      }
    }

#define INSN(NAME, op, funct3, funct7)                                              \
  void NAME(Register Rd, Register Rs1, Aqrl memory_order = relaxed) {               \
    unsigned insn = 0;                                                              \
    uint32_t val = memory_order & 0x3;                                              \
    patch((address)&insn, 6, 0, op);                                                \
    patch((address)&insn, 14, 12, funct3);                                          \
    patch_reg((address)&insn, 7, Rd);                                               \
    patch_reg((address)&insn, 15, Rs1);                                             \
    patch((address)&insn, 25, 20, 0b00000);                                         \
    patch((address)&insn, 31, 27, funct7);                                          \
    patch((address)&insn, 26, 25, val);                                             \
    emit_int32(insn);                                                               \
  }

    INSN(lr_w, 0b0101111, 0b010, 0b00010);
    INSN(lr_d, 0b0101111, 0b011, 0b00010);

#undef INSN

#define INSN(NAME, op, funct3, funct7)                                                      \
  void NAME(Register Rd, Register Rs1, Register Rs2, Aqrl memory_order = relaxed) {         \
    unsigned insn = 0;                                                                      \
    uint32_t val = memory_order & 0x3;                                                      \
    patch((address)&insn, 6, 0, op);                                                        \
    patch((address)&insn, 14, 12, funct3);                                                  \
    patch_reg((address)&insn, 7, Rd);                                                       \
    patch_reg((address)&insn, 15, Rs2);                                                     \
    patch_reg((address)&insn, 20, Rs1);                                                     \
    patch((address)&insn, 31, 27, funct7);                                                  \
    patch((address)&insn, 26, 25, val);                                                     \
    emit_int32(insn);                                                                       \
  }

    INSN(sc_w, 0b0101111, 0b010, 0b00011);
    INSN(sc_d, 0b0101111, 0b011, 0b00011);
#undef INSN

    void load_primitive(Register addr,
                        enum operand_size size) {
      switch (size) {
        case int64:
          ld(t0, addr, 0);
          break;
        case int32:
          lw(t0, addr, 0);
          break;
        case uint32:
          lw(t0, addr, 0);
          clear_upper_bits(t0, 32);
          break;
        default:
          assert(false);
      }
    }

    void store_primitive(Register addr,
                         Register new_val,
                         enum operand_size size) {
      switch (size) {
        case int64:
          sd(new_val, addr, 0);
          break;
        case int32:
        case uint32:
          sw(new_val, addr, 0);
          break;
        default:
          assert(false);
      }
    }

    void load_reserved(Register addr,
                       enum operand_size size,
                       Assembler::Aqrl acquire) {
      switch (size) {
        case int64:
          lr_d(t0, addr, acquire);
          break;
        case int32:
          lr_w(t0, addr, acquire);
          break;
        case uint32:
          lr_w(t0, addr, acquire);
          clear_upper_bits(t0, 32);
          break;
        default:
          assert(false);
      }
    }

    void store_conditional(Register addr,
                           Register new_val,
                           enum operand_size size,
                           Assembler::Aqrl release) {
      switch (size) {
        case int64:
          sc_d(t0, new_val, addr, release);
          break;
        case int32:
        case uint32:
          sc_w(t0, new_val, addr, release);
          break;
        default:
          assert(false);
      }
    }

#define INSN(NAME, op, funct3)                                                                           \
  void NAME(Register Rs1, Register Rs2, const int64_t offset, bool compressed = true) {                  \
    unsigned insn = 0;                                                                                   \
    assert(is_imm_in_range(offset, 12, 1) && "offset is invalid.");                                      \
    uint32_t val  = offset & 0x1fff;                                                                     \
    uint32_t val11 = (val >> 11) & 0x1;                                                                  \
    uint32_t val12 = (val >> 12) & 0x1;                                                                  \
    uint32_t low  = (val >> 1) & 0xf;                                                                    \
    uint32_t high = (val >> 5) & 0x3f;                                                                   \
    patch((address)&insn, 6, 0, op);                                                                     \
    patch((address)&insn, 14, 12, funct3);                                                               \
    patch_reg((address)&insn, 15, Rs1);                                                                  \
    patch_reg((address)&insn, 20, Rs2);                                                                  \
    patch((address)&insn, 7, val11);                                                                     \
    patch((address)&insn, 11, 8, low);                                                                   \
    patch((address)&insn, 30, 25, high);                                                                 \
    patch((address)&insn, 31, val12);                                                                    \
    emit_int32(insn);                                                                                    \
  }                                                                                                      \
  void NAME(Register Rs1, Register Rs2, const address dest, bool compressed = true) {                    \
    assert(dest != NULL);                                                                                \
    int64_t offset = (dest - pc());                                                                      \
    assert(is_imm_in_range(offset, 12, 1) && "offset is invalid.");                                      \
    NAME(Rs1, Rs2, offset, compressed);                                                                  \
  }                                                                                                      \

    INSN(beq,  0b1100011, 0b000);
    INSN(bne,  0b1100011, 0b001);
    INSN(bge,  0b1100011, 0b101);
    INSN(bgeu, 0b1100011, 0b111);
    INSN(blt,  0b1100011, 0b100);
    INSN(bltu, 0b1100011, 0b110);

#undef INSN

#define INSN(NAME, NEG_INSN)                                                                            \
  void NAME(Register Rs1, Register Rs2, Label &L, bool is_far = false, bool compressed = false) {       \
    do_label(Rs1, Rs2, L, &Assembler::NAME, &Assembler::NEG_INSN, is_far, compressed);                  \
  }

    INSN(beq,  bne);
    INSN(bne,  beq);
    INSN(blt,  bge);
    INSN(bge,  blt);
    INSN(bltu, bgeu);
    INSN(bgeu, bltu);

#undef INSN

#define INSN(NAME, NEG_INSN)                                                               \
  void NAME(Register Rs, Register Rt, const address &dest, bool compressed) {              \
    NEG_INSN(Rt, Rs, dest, compressed);                                                    \
  }                                                                                        \
  void NAME(Register Rs, Register Rt, Label &l, bool is_far, bool compressed) {            \
    NEG_INSN(Rt, Rs, l, is_far, compressed);                                               \
  }

    INSN(bgt,  blt);
    INSN(ble,  bge);
    INSN(bgtu, bltu);
    INSN(bleu, bgeu);
#undef INSN

#define INSN(NAME)                                                                    \
  void NAME##z(Register Rs, const address &dest, bool compressed = true) {            \
    NAME(Rs, zr, dest, compressed);                                                   \
  }                                                                                   \
  void NAME##z(Register Rs, Label &l, bool is_far = false, bool compressed = true) {  \
    NAME(Rs, zr, l, is_far, compressed);                                              \
  }                                                                                   \

    INSN(beq);
    INSN(bne);
    INSN(blt);
    INSN(ble);
    INSN(bge);
    INSN(bgt);

#undef INSN

    void mv(Register Rd, address addr) {
      // Here in case of use with relocation, use fix length instruction
      // movptr instead of li
      movptr(Rd, addr);
    }

    void movptr(Register Rd, uintptr_t imm64) {
      movptr(Rd, (address)imm64);
    }

    void movptr(Register Rd, address addr) {
      int offset = 0;
      movptr_with_offset(Rd, addr, offset);
      addi(Rd, Rd, offset);
    }

    void movptr_with_offset(Register Rd, address addr, int32_t &offset) {
      uintptr_t imm64 = (uintptr_t)addr;
      assert((is_unsigned_imm_in_range(imm64, 47, 0) || (imm64 == (uintptr_t)-1)) &&
             "48-bit overflow in address constant");
      // Load upper 32 bits
      int32_t imm = imm64 >> 16;
      int64_t upper = imm, lower = imm;
      lower = (lower << 52) >> 52;
      upper -= lower;
      upper = (int32_t)upper;
      lui(Rd, upper);
      addi(Rd, Rd, lower);

      // Load the rest 16 bits.
      slli(Rd, Rd, 11);
      addi(Rd, Rd, (imm64 >> 5) & 0x7ff);
      slli(Rd, Rd, 5);

      // This offset will be used by following jalr/ld.
      offset = imm64 & 0x1f;
    }

    void li32(Register Rd, int32_t imm) {
      // int32_t is in range 0x8000 0000 ~ 0x7fff ffff, and imm[31] is the sign bit
      int64_t upper = imm, lower = imm;
//      {
//        // equal as below
//        upper = (((imm + 0x800) >> 12) & 0xFFFFF) << 12;
//        lower = (imm << 20) >> 20;
//      }
      lower = (imm << 20) >> 20;
      upper -= lower;
      upper = (int32_t)upper;
      // lui Rd, imm[31:12] + imm[11]
      lui(Rd, upper);
      addiw(Rd, Rd, lower);
    }

    void li64(Register Rd, int64_t imm) {
      // Load upper 32 bits. upper = imm[63:32], but if imm[31] == 1 or
      // (imm[31:28] == 0x7ff && imm[19] == 1), upper = imm[63:32] + 1.
      int64_t lower = imm & 0xffffffff;
      lower -= ((lower << 44) >> 44);
      int64_t tmp_imm = ((uint64_t)(imm & 0xffffffff00000000)) + (uint64_t)lower;
      int32_t upper = (tmp_imm - (int32_t)lower) >> 32;

      // Load upper 32 bits
      int64_t up = upper, lo = upper;
      lo = (lo << 52) >> 52;
      up -= lo;
      up = (int32_t)up;
      lui(Rd, up);
      addi(Rd, Rd, lo);

      // Load the rest 32 bits.
      slli(Rd, Rd, 12);
      addi(Rd, Rd, (int32_t)lower >> 20);
      slli(Rd, Rd, 12);
      lower = ((int32_t)imm << 12) >> 20;
      addi(Rd, Rd, lower);
      slli(Rd, Rd, 8);
      lower = imm & 0xff;
      addi(Rd, Rd, lower);
    }

    void mv(Register Rd, Register Rs) {
      if (Rd != Rs) {
        addi(Rd, Rs, 0);
      }
    }

    // from V8: assembler-riscv64.cc
    void li_ptr(Register rd, int64_t imm) {
      // Initialize rd with an address
      // Pointers are 48 bits
      // 6 fixed instructions are generated
      // assert((imm & 0xfff0000000000000ll) == 0 && "sanity");
      int64_t a6 = imm & 0x3f;                      // bits 0:5. 6 bits
      int64_t b11 = (imm >> 6) & 0x7ff;             // bits 6:11. 11 bits
      int64_t high_31 = (imm >> 17) & 0x7fffffff;   // 31 bits  // Support -1: remove the '& 0x7fffffff'.
      int64_t high_20 = ((high_31 + 0x800) >> 12);  // 19 bits
      printf("0x%lx 0x%lx\n", (high_31) >> 12, high_20); // <>, 0x4000_0000
      fflush(stdout);
      int64_t low_12 = ((high_31 & 0xfff) << 52) >> 52;             // 12 bits
      lui(rd, ((int32_t)high_20) << 12);
      addi(rd, rd, low_12);  // 31 bits in rd.
      slli(rd, rd, 11);      // Space for next 11 bis
      ori(rd, rd, b11);      // 11 bits are put in. 42 bit in rd
      slli(rd, rd, 6);       // Space for next 6 bits
      ori(rd, rd, a6);       // 6 bits are put in. 48 bis in rd
    }

    // copied from V8's version
    void movptr_with_offset2(Register Rd, address addr, int32_t &offset) {
      uintptr_t imm64 = (uintptr_t)addr;
      assert((is_unsigned_imm_in_range(imm64, 47, 0) || (imm64 == (uintptr_t)-1)) &&
             "48-bit overflow in address constant");
      // Load upper 31 bits
      int32_t imm = imm64 >> 17;
      int64_t upper = imm, lower = imm;
      lower = (lower << 52) >> 52;
      upper -= lower;
      upper = (int32_t)upper;
      lui(Rd, upper);
      addi(Rd, Rd, lower);

      // Load the rest 17 bits.
      slli(Rd, Rd, 11);
      addi(Rd, Rd, (imm64 >> 6) & 0x7ff);
      slli(Rd, Rd, 6);

      // This offset will be used by following jalr/ld.
      offset = imm64 & 0x3f;
    }
    void movptr2(Register Rd, address addr) {
      int offset = 0;
      movptr_with_offset2(Rd, addr, offset);
      addi(Rd, Rd, offset);
    }

    void cmpxchg(Register addr, Register expected,
                 Register new_val,
                 enum operand_size size,
                 Assembler::Aqrl acquire, Assembler::Aqrl release,
                 Register result, bool result_as_bool) {
      assert(size != int8 && size != int16 && "unsupported operand size");

      Label retry_load, done, ne_done;
      bind(retry_load);
      if (UsePrimitive) {
        load_primitive(addr, size);
      } else {
        load_reserved(addr, size, acquire);
      }
      bne(t0, expected, ne_done);
      if (UsePrimitive) {
        store_primitive(addr, new_val, size);
        li(t0, 0);
      } else {
        store_conditional(addr, new_val, size, release);
      }
      bnez(t0, retry_load);

      // equal, succeed
      if (result_as_bool) {
        li(result, 1);
      } else {
        mv(result, expected);
      }
      j(done);

      // not equal, failed
      bind(ne_done);
      if (result_as_bool) {
        mv(result, zr);
      } else {
        mv(result, t0);
      }

      bind(done);
    }

    int bitset_to_regs(unsigned int bitset, unsigned char* regs) {
      int count = 0;
      // Scan bitset to accumulate register pairs
      for (int reg = 31; reg >= 0; reg--) {
        if ((1U << 31) & bitset) {
          regs[count++] = reg;
        }
        bitset <<= 1;
      }
      return count;
    }

    int push_reg(unsigned int bitset, Register stack) {
      unsigned char regs[32];
      int count = bitset_to_regs(bitset, regs);
      // reserve one slot to align for odd count
      int offset = is_even(count) ? 0 : wordSize;

      if (count) {
        addi(stack, stack, - count * wordSize - offset);
      }
      for (int i = count - 1; i >= 0; i--) {
        sd(as_Register(regs[i]), stack, (count - 1 - i) * wordSize + offset);
      }

      return count;
    }

    int pop_reg(unsigned int bitset, Register stack) {
      unsigned char regs[32];
      int count = bitset_to_regs(bitset, regs);
      // reserve one slot to align for odd count
      int offset = is_even(count) ? 0 : wordSize;

      for (int i = count - 1; i >= 0; i--) {
        ld(as_Register(regs[i]), stack, (count - 1 - i) * wordSize + offset);
      }

      if (count) {
        addi(stack, stack, count * wordSize + offset);
      }

      return count;
    }

    void pusha() {
      push_reg(0xFFFFFFFF, sp);
    }

    void popa() {
      pop_reg(0xFFFFFFFF, sp);
    }

    void pusha_excluding_a0() {
      push_reg(0xFFFFFBFF, sp);
    }

    void popa_excluding_a0() {
      pop_reg(0xFFFFFBFF, sp);
    }

    void push_reg(RegSet regs, Register stack) { if (regs.bits()) { push_reg(regs.bits(), stack); } }
    void pop_reg(RegSet regs, Register stack) { if (regs.bits()) { pop_reg(regs.bits(), stack); } }

    void push_reg_excluding(RegSet exclude, Register stack) { push_reg(RegSet::range(x0, x31) - exclude, stack); }
    void pop_reg_excluding(RegSet exclude, Register stack) { pop_reg(RegSet::range(x0, x31) - exclude, stack); }

    void enter() {
      addi(sp, sp, - 2 * wordSize);
      sd(ra, sp, wordSize);
      sd(fp, sp, 0);
      addi(fp, sp, 2 * wordSize);
    }

    void leave() {
      addi(sp, fp, - 2 * wordSize);
      ld(fp, sp, 0);
      ld(ra, sp, wordSize);
      addi(sp, sp, 2 * wordSize);
    }

    // -----------------------  RVC  ----------------------------

    // patch a 16-bit instruction.
    static void c_patch(address a, unsigned msb, unsigned lsb, uint16_t val) {
      assert(a != NULL);
      assert(msb >= lsb && msb <= 15);
      unsigned nbits = msb - lsb + 1;
      assert(val < (1U << nbits) && "Field too big for insn");
      uint16_t mask = (1U << nbits) - 1;
      val <<= lsb;
      mask <<= lsb;
      uint16_t target = *(uint16_t *)a;
      target &= ~mask;
      target |= val;
      *(uint16_t *)a = target;
    }

    static void c_patch(address a, unsigned bit, uint16_t val) {
      c_patch(a, bit, bit, val);
    }

    // patch a 16-bit instruction with a general purpose register ranging [0, 31] (5 bits)
    static void c_patch_reg(address a, unsigned lsb, Register reg) {
      c_patch(a, lsb + 4, lsb, reg->encoding_nocheck());
    }

    // patch a 16-bit instruction with a general purpose register ranging [8, 15] (3 bits)
    static void c_patch_compressed_reg(address a, unsigned lsb, Register reg) {
      c_patch(a, lsb + 2, lsb, reg->compressed_encoding_nocheck());
    }

    // RVC: Compressed Instructions
    void c_nop() {
      c_addi(x0, 0);
    }

#define assert_cond assert

#define INSN(NAME, funct3, op)                                                               \
  void NAME(Register Rd_Rs1, int32_t imm) {                                                  \
    assert_cond(is_imm_in_range(imm, 6, 0));                                                 \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch((address)&insn, 6, 2, (imm & right_n_bits(5)));                                  \
    c_patch_reg((address)&insn, 7, Rd_Rs1);                                                  \
    c_patch((address)&insn, 12, 12, (imm & nth_bit(5)) >> 5);                                \
    c_patch((address)&insn, 15, 13, funct3);                                                 \
    emit_int16(insn);                                                                        \
  }

    INSN(c_addi,   0b000, 0b01);
    INSN(c_addiw,  0b001, 0b01);

#undef INSN

#define INSN(NAME, funct3, op)                                                               \
  void NAME(int32_t imm) {                                                                   \
    assert_cond(is_imm_in_range(imm, 10, 0));                                                \
    assert_cond((imm & 0b1111) == 0);                                                        \
    assert_cond(imm != 0);                                                                   \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch((address)&insn, 2, 2, (imm & nth_bit(5)) >> 5);                                  \
    c_patch((address)&insn, 4, 3, (imm & right_n_bits(9)) >> 7);                             \
    c_patch((address)&insn, 5, 5, (imm & nth_bit(6)) >> 6);                                  \
    c_patch((address)&insn, 6, 6, (imm & nth_bit(4)) >> 4);                                  \
    c_patch_reg((address)&insn, 7, sp);                                                      \
    c_patch((address)&insn, 12, 12, (imm & nth_bit(9)) >> 9);                                \
    c_patch((address)&insn, 15, 13, funct3);                                                 \
    emit_int16(insn);                                                                        \
  }

    INSN(c_addi16sp, 0b011, 0b01);

#undef INSN

#define INSN(NAME, funct3, op)                                                               \
  void NAME(Register Rd, uint32_t uimm) {                                                    \
    assert_cond(is_unsigned_imm_in_range(uimm, 10, 0));                                      \
    assert_cond((uimm & 0b11) == 0);                                                         \
    assert_cond(uimm != 0);                                                                  \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch_compressed_reg((address)&insn, 2, Rd);                                           \
    c_patch((address)&insn, 5, 5, (uimm & nth_bit(3)) >> 3);                                 \
    c_patch((address)&insn, 6, 6, (uimm & nth_bit(2)) >> 2);                                 \
    c_patch((address)&insn, 10, 7, (uimm & right_n_bits(10)) >> 6);                          \
    c_patch((address)&insn, 12, 11, (uimm & right_n_bits(6)) >> 4);                          \
    c_patch((address)&insn, 15, 13, funct3);                                                 \
    emit_int16(insn);                                                                        \
  }

    INSN(c_addi4spn, 0b000, 0b00);

#undef INSN

#define INSN(NAME, funct3, op)                                                               \
  void NAME(Register Rd_Rs1, uint32_t shamt) {                                               \
    assert_cond(is_unsigned_imm_in_range(shamt, 6, 0));                                      \
    assert_cond(shamt != 0);                                                                 \
    assert_cond(Rd_Rs1 != x0);                                                               \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch((address)&insn, 6, 2, (shamt & right_n_bits(5)));                                \
    c_patch_reg((address)&insn, 7, Rd_Rs1);                                                  \
    c_patch((address)&insn, 12, 12, (shamt & nth_bit(5)) >> 5);                              \
    c_patch((address)&insn, 15, 13, funct3);                                                 \
    emit_int16(insn);                                                                        \
  }

    INSN(c_slli, 0b000, 0b10);

#undef INSN

#define INSN(NAME, funct3, funct2, op)                                                       \
  void NAME(Register Rd_Rs1, uint32_t shamt) {                                               \
    assert_cond(is_unsigned_imm_in_range(shamt, 6, 0));                                      \
    assert_cond(shamt != 0);                                                                 \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch((address)&insn, 6, 2, (shamt & right_n_bits(5)));                                \
    c_patch_compressed_reg((address)&insn, 7, Rd_Rs1);                                       \
    c_patch((address)&insn, 11, 10, funct2);                                                 \
    c_patch((address)&insn, 12, 12, (shamt & nth_bit(5)) >> 5);                              \
    c_patch((address)&insn, 15, 13, funct3);                                                 \
    emit_int16(insn);                                                                        \
  }

    INSN(c_srli, 0b100, 0b00, 0b01);
    INSN(c_srai, 0b100, 0b01, 0b01);

#undef INSN

#define INSN(NAME, funct3, funct2, op)                                                       \
  void NAME(Register Rd_Rs1, int32_t imm) {                                                  \
    assert_cond(is_imm_in_range(imm, 6, 0));                                                 \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch((address)&insn, 6, 2, (imm & right_n_bits(5)));                                  \
    c_patch_compressed_reg((address)&insn, 7, Rd_Rs1);                                       \
    c_patch((address)&insn, 11, 10, funct2);                                                 \
    c_patch((address)&insn, 12, 12, (imm & nth_bit(5)) >> 5);                                \
    c_patch((address)&insn, 15, 13, funct3);                                                 \
    emit_int16(insn);                                                                        \
  }

    INSN(c_andi, 0b100, 0b10, 0b01);

#undef INSN

#define INSN(NAME, funct6, funct2, op)                                                       \
  void NAME(Register Rd_Rs1, Register Rs2) {                                                 \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch_compressed_reg((address)&insn, 2, Rs2);                                          \
    c_patch((address)&insn, 6, 5, funct2);                                                   \
    c_patch_compressed_reg((address)&insn, 7, Rd_Rs1);                                       \
    c_patch((address)&insn, 15, 10, funct6);                                                 \
    emit_int16(insn);                                                                        \
  }

    INSN(c_sub,  0b100011, 0b00, 0b01);
    INSN(c_xor,  0b100011, 0b01, 0b01);
    INSN(c_or,   0b100011, 0b10, 0b01);
    INSN(c_and,  0b100011, 0b11, 0b01);
    INSN(c_subw, 0b100111, 0b00, 0b01);
    INSN(c_addw, 0b100111, 0b01, 0b01);

#undef INSN

#define INSN(NAME, funct4, op)                                                               \
  void NAME(Register Rd_Rs1, Register Rs2) {                                                 \
    assert_cond(Rd_Rs1 != x0);                                                               \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch_reg((address)&insn, 2, Rs2);                                                     \
    c_patch_reg((address)&insn, 7, Rd_Rs1);                                                  \
    c_patch((address)&insn, 15, 12, funct4);                                                 \
    emit_int16(insn);                                                                        \
  }

    INSN(c_mv,  0b1000, 0b10);
    INSN(c_add, 0b1001, 0b10);

#undef INSN

#define INSN(NAME, funct4, op)                                                               \
  void NAME(Register Rs1) {                                                                  \
    assert_cond(Rs1 != x0);                                                                  \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch_reg((address)&insn, 2, x0);                                                      \
    c_patch_reg((address)&insn, 7, Rs1);                                                     \
    c_patch((address)&insn, 15, 12, funct4);                                                 \
    emit_int16(insn);                                                                        \
  }

    INSN(c_jr,   0b1000, 0b10);
    INSN(c_jalr, 0b1001, 0b10);

#undef INSN

    typedef void (Assembler::* j_c_insn)(address dest);
    typedef void (Assembler::* compare_and_branch_c_insn)(Register Rs1, address dest);

    void wrap_label(Label &L, j_c_insn insn) {
      if (L.is_bound()) {
        (this->*insn)(L.target_address());
      } else {
        L.set_patch_address(_end);
        (this->*insn)(pc());
      }
    }

    void wrap_label(Label &L, Register r, compare_and_branch_c_insn insn) {
      if (L.is_bound()) {
        (this->*insn)(r, L.target_address());
      } else {
        L.set_patch_address(_end);
        (this->*insn)(r, pc());
      }
    }

#define INSN(NAME, funct3, op)                                                               \
  void NAME(int32_t offset) {                                                                \
    assert_cond(is_imm_in_range(offset, 11, 1));                                             \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch((address)&insn, 2, 2, (offset & nth_bit(5)) >> 5);                               \
    c_patch((address)&insn, 5, 3, (offset & right_n_bits(4)) >> 1);                          \
    c_patch((address)&insn, 6, 6, (offset & nth_bit(7)) >> 7);                               \
    c_patch((address)&insn, 7, 7, (offset & nth_bit(6)) >> 6);                               \
    c_patch((address)&insn, 8, 8, (offset & nth_bit(10)) >> 10);                             \
    c_patch((address)&insn, 10, 9, (offset & right_n_bits(10)) >> 8);                        \
    c_patch((address)&insn, 11, 11, (offset & nth_bit(4)) >> 4);                             \
    c_patch((address)&insn, 12, 12, (offset & nth_bit(11)) >> 11);                           \
    c_patch((address)&insn, 15, 13, funct3);                                                 \
    emit_int16(insn);                                                                        \
  }                                                                                          \
  void NAME(address dest) {                                                                  \
    assert_cond(dest != NULL);                                                               \
    int64_t distance = dest - pc();                                                          \
    assert_cond(is_imm_in_range(distance, 11, 1));                                           \
    c_j(distance);                                                                           \
  }                                                                                          \
  void NAME(Label &L) {                                                                      \
    wrap_label(L, &Assembler::NAME);                                                         \
  }

    INSN(c_j, 0b101, 0b01);

#undef INSN

#define INSN(NAME, funct3, op)                                                               \
  void NAME(Register Rs1, int32_t imm) {                                                     \
    assert_cond(is_imm_in_range(imm, 8, 1));                                                 \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch((address)&insn, 2, 2, (imm & nth_bit(5)) >> 5);                                  \
    c_patch((address)&insn, 4, 3, (imm & right_n_bits(3)) >> 1);                             \
    c_patch((address)&insn, 6, 5, (imm & right_n_bits(8)) >> 6);                             \
    c_patch_compressed_reg((address)&insn, 7, Rs1);                                          \
    c_patch((address)&insn, 11, 10, (imm & right_n_bits(5)) >> 3);                           \
    c_patch((address)&insn, 12, 12, (imm & nth_bit(8)) >> 8);                                \
    c_patch((address)&insn, 15, 13, funct3);                                                 \
    emit_int16(insn);                                                                        \
  }                                                                                          \
  void NAME(Register Rs1, address dest) {                                                    \
    assert_cond(dest != NULL);                                                               \
    int64_t distance = dest - pc();                                                          \
    assert_cond(is_imm_in_range(distance, 8, 1));                                            \
    NAME(Rs1, distance);                                                                     \
  }                                                                                          \
  void NAME(Register Rs1, Label &L) {                                                        \
    wrap_label(L, Rs1, &Assembler::NAME);                                                    \
  }

    INSN(c_beqz, 0b110, 0b01);
    INSN(c_bnez, 0b111, 0b01);

#undef INSN

#define INSN(NAME, funct3, op)                                                               \
  void NAME(Register Rd, int32_t imm) {                                                      \
    assert_cond(is_imm_in_range(imm, 18, 0));                                                \
    assert_cond((imm & 0xfff) == 0);                                                         \
    assert_cond(imm != 0);                                                                   \
    assert_cond(Rd != x0 && Rd != x2);                                                       \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch((address)&insn, 6, 2, (imm & right_n_bits(17)) >> 12);                           \
    c_patch_reg((address)&insn, 7, Rd);                                                      \
    c_patch((address)&insn, 12, 12, (imm & nth_bit(17)) >> 17);                              \
    c_patch((address)&insn, 15, 13, funct3);                                                 \
    emit_int16(insn);                                                                        \
  }

    INSN(c_lui, 0b011, 0b01);

#undef INSN

#define INSN(NAME, funct3, op)                                                               \
  void NAME(Register Rd, int32_t imm) {                                                      \
    assert_cond(is_imm_in_range(imm, 6, 0));                                                 \
    assert_cond(Rd != x0);                                                                   \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch((address)&insn, 6, 2, (imm & right_n_bits(5)));                                  \
    c_patch_reg((address)&insn, 7, Rd);                                                      \
    c_patch((address)&insn, 12, 12, (imm & right_n_bits(6)) >> 5);                           \
    c_patch((address)&insn, 15, 13, funct3);                                                 \
    emit_int16(insn);                                                                        \
  }

    INSN(c_li, 0b010, 0b01);

#undef INSN

#define INSN(NAME, funct3, op)                                                               \
  void NAME(Register Rd, uint32_t uimm) {                                                    \
    assert_cond(is_unsigned_imm_in_range(uimm, 9, 0));                                       \
    assert_cond((uimm & 0b111) == 0);                                                        \
    assert_cond(Rd != x0);                                                                   \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch((address)&insn, 4, 2, (uimm & right_n_bits(9)) >> 6);                            \
    c_patch((address)&insn, 6, 5, (uimm & right_n_bits(5)) >> 3);                            \
    c_patch_reg((address)&insn, 7, Rd);                                                      \
    c_patch((address)&insn, 12, 12, (uimm & nth_bit(5)) >> 5);                               \
    c_patch((address)&insn, 15, 13, funct3);                                                 \
    emit_int16(insn);                                                                        \
  }

    INSN(c_ldsp,  0b011, 0b10);

#undef INSN

#define INSN(NAME, funct3, op, REGISTER_TYPE)                                                \
  void NAME(REGISTER_TYPE Rd_Rs2, Register Rs1, uint32_t uimm) {                             \
    assert_cond(is_unsigned_imm_in_range(uimm, 8, 0));                                       \
    assert_cond((uimm & 0b111) == 0);                                                        \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch_compressed_reg((address)&insn, 2, Rd_Rs2);                                       \
    c_patch((address)&insn, 6, 5, (uimm & right_n_bits(8)) >> 6);                            \
    c_patch_compressed_reg((address)&insn, 7, Rs1);                                          \
    c_patch((address)&insn, 12, 10, (uimm & right_n_bits(6)) >> 3);                          \
    c_patch((address)&insn, 15, 13, funct3);                                                 \
    emit_int16(insn);                                                                        \
  }

    INSN(c_ld,  0b011, 0b00, Register);
    INSN(c_sd,  0b111, 0b00, Register);

#undef INSN

#define INSN(NAME, funct3, op, REGISTER_TYPE)                                                \
  void NAME(REGISTER_TYPE Rs2, uint32_t uimm) {                                              \
    assert_cond(is_unsigned_imm_in_range(uimm, 9, 0));                                       \
    assert_cond((uimm & 0b111) == 0);                                                        \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch_reg((address)&insn, 2, Rs2);                                                     \
    c_patch((address)&insn, 9, 7, (uimm & right_n_bits(9)) >> 6);                            \
    c_patch((address)&insn, 12, 10, (uimm & right_n_bits(6)) >> 3);                          \
    c_patch((address)&insn, 15, 13, funct3);                                                 \
    emit_int16(insn);                                                                        \
  }

    INSN(c_sdsp,  0b111, 0b10, Register);

#undef INSN

#define INSN(NAME, funct3, op)                                                               \
  void NAME(Register Rs2, uint32_t uimm) {                                                   \
    assert_cond(is_unsigned_imm_in_range(uimm, 8, 0));                                       \
    assert_cond((uimm & 0b11) == 0);                                                         \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch_reg((address)&insn, 2, Rs2);                                                     \
    c_patch((address)&insn, 8, 7, (uimm & right_n_bits(8)) >> 6);                            \
    c_patch((address)&insn, 12, 9, (uimm & right_n_bits(6)) >> 2);                           \
    c_patch((address)&insn, 15, 13, funct3);                                                 \
    emit_int16(insn);                                                                        \
  }

    INSN(c_swsp, 0b110, 0b10);

#undef INSN

#define INSN(NAME, funct3, op)                                                               \
  void NAME(Register Rd, uint32_t uimm) {                                                    \
    assert_cond(is_unsigned_imm_in_range(uimm, 8, 0));                                       \
    assert_cond((uimm & 0b11) == 0);                                                         \
    assert_cond(Rd != x0);                                                                   \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch((address)&insn, 3, 2, (uimm & right_n_bits(8)) >> 6);                            \
    c_patch((address)&insn, 6, 4, (uimm & right_n_bits(5)) >> 2);                            \
    c_patch_reg((address)&insn, 7, Rd);                                                      \
    c_patch((address)&insn, 12, 12, (uimm & nth_bit(5)) >> 5);                               \
    c_patch((address)&insn, 15, 13, funct3);                                                 \
    emit_int16(insn);                                                                        \
  }

    INSN(c_lwsp, 0b010, 0b10);

#undef INSN

#define INSN(NAME, funct3, op)                                                               \
  void NAME(Register Rd_Rs2, Register Rs1, uint32_t uimm) {                                  \
    assert_cond(is_unsigned_imm_in_range(uimm, 7, 0));                                       \
    assert_cond((uimm & 0b11) == 0);                                                         \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch_compressed_reg((address)&insn, 2, Rd_Rs2);                                       \
    c_patch((address)&insn, 5, 5, (uimm & nth_bit(6)) >> 6);                                 \
    c_patch((address)&insn, 6, 6, (uimm & nth_bit(2)) >> 2);                                 \
    c_patch_compressed_reg((address)&insn, 7, Rs1);                                          \
    c_patch((address)&insn, 12, 10, (uimm & right_n_bits(6)) >> 3);                          \
    c_patch((address)&insn, 15, 13, funct3);                                                 \
    emit_int16(insn);                                                                        \
  }

    INSN(c_lw, 0b010, 0b00);
    INSN(c_sw, 0b110, 0b00);

#undef INSN

#define INSN(NAME, funct3, op)                                                               \
  void NAME() {                                                                              \
    uint16_t insn = 0;                                                                       \
    c_patch((address)&insn, 1, 0, op);                                                       \
    c_patch((address)&insn, 11, 2, 0x0);                                                     \
    c_patch((address)&insn, 12, 12, 0b1);                                                    \
    c_patch((address)&insn, 15, 13, funct3);                                                 \
    emit_int16(insn);                                                                        \
  }

    INSN(c_ebreak, 0b100, 0b10);

    void illegal() {
      emit_int32(0);
    }

    void c_illegal() {
      emit_int16(0);
    }

#undef INSN

#include "assembler_riscv_selfmade.hpp"   // here include our self-made instructions

// end rv insts
};

static address get_target_of_movptr(address insn_addr) {
          assert_cond(insn_addr != NULL);
  intptr_t target_address = ((int64_t)Assembler::sextract(((unsigned*)insn_addr)[0], 31, 12)) << 12;    // Lui.
  // printf("[get_target_of_movptr1] " INTPTR_FORMAT "\n", p2i((address)target_address));
  // fflush(stdout);
  target_address += (int64_t)Assembler::sextract(((unsigned*)insn_addr)[1], 31, 20);                                // Addiw.
  // and sign extend the bit 31 as addiw describes
  target_address  = (((int64_t) target_address) << 32) >> 32;
  // printf("[get_target_of_movptr2] " INTPTR_FORMAT "\n", p2i((address)target_address));
  // fflush(stdout);
  target_address <<= 11;                                                                                                         // Slli.
  target_address += (int64_t)Assembler::sextract(((unsigned*)insn_addr)[3], 31, 20);          // Addi.
  target_address <<= 5;                                                                                                          // Slli.
  target_address += (int64_t)Assembler::sextract(((unsigned*)insn_addr)[5], 31, 20);          // Addi/Jalr/Load.
  assert((Assembler::is_unsigned_imm_in_range(target_address, 47, 0) || (target_address == (uintptr_t)-1)) &&
         "48-bit overflow in address constant");
  return (address) target_address;
}

static int patch_addr_in_movptr(address branch, address target) {
  uintptr_t imm64 = (uintptr_t)target;
  assert((Assembler::is_unsigned_imm_in_range(imm64, 47, 0) || (imm64 == (uintptr_t)-1)) && "48-bit overflow in address constant");
  const int size = 6 * 4;  // lui + addiw + slli(C) + addi + slli(C) + addi/jalr/load
  int32_t lower = ((intptr_t)target << 36) >> 36;
  int64_t upper = ((intptr_t)target - lower) >> 28;
  Assembler::patch(branch + 0,  31, 12, upper & 0xfffff);                       // Lui.             target[47:28] + target[27] ==> branch[31:12]
  Assembler::patch(branch + 4,  31, 20, (lower >> 16) & 0xfff);                 // Addiw.           target[27:16] ==> branch[31:20]
  Assembler::patch(branch + 12, 31, 20, (lower >> 5) & 0x7ff);                  // Addi.            target[15: 5] ==> branch[31:20]
  Assembler::patch(branch + 20, 31, 20, lower & 0x1f);                          // Addi/Jalr/Load.  target[ 4: 0] ==> branch[31:20]
  return size;
}

static address get_target_of_movptr2(address insn_addr) {
  assert_cond(insn_addr != NULL);
  intptr_t target_address = (((int64_t)Assembler::sextract(((unsigned*)insn_addr)[0], 31, 12)) & 0xfffff) << 29;    // Lui.
  target_address += ((int64_t)Assembler::sextract(((unsigned*)insn_addr)[1], 31, 20)) << 17;                        // Addi.
  target_address += ((int64_t)Assembler::sextract(((unsigned*)insn_addr)[3], 31, 20)) << 6;                         // Addi.
  target_address += ((int64_t)Assembler::sextract(((unsigned*)insn_addr)[5], 31, 20));                              // Addi/Jalr/Load.
  return (address) target_address;
}

static int patch_addr_in_movptr2(address branch, address target) {
  uintptr_t imm64 = (uintptr_t)target;
  assert((Assembler::is_unsigned_imm_in_range(imm64, 47, 0) || (imm64 == (uintptr_t)-1)) && "48-bit overflow in address constant");
  const int size = 6 * 4;  // lui + addiw + slli(C) + addi + slli(C) + addi/jalr/load
  int32_t lower = ((intptr_t)target << 35) >> 35;
  int64_t upper = ((intptr_t)target - lower) >> 29;
  Assembler::patch(branch + 0,  31, 12, upper & 0xfffff);                       // Lui.             target[47:28] + target[27] ==> branch[31:12]
  Assembler::patch(branch + 4,  31, 20, (lower >> 17) & 0xfff);                 // Addiw.           target[27:16] ==> branch[31:20]
  Assembler::patch(branch + 12, 31, 20, (lower >> 6) & 0x7ff);                  // Addi.            target[15: 5] ==> branch[31:20]
  Assembler::patch(branch + 20, 31, 20, lower & 0x3f);                          // Addi/Jalr/Load.  target[ 4: 0] ==> branch[31:20]
  return size;
}

static int patch_imm_in_li32(address branch, int32_t target) {
  const int LI32_INSTRUCTIONS_NUM = 2;                                          // lui + addiw
  int64_t upper = (intptr_t)target;
  int32_t lower = (((int32_t)target) << 20) >> 20;
  upper -= lower;
  upper = (int32_t)upper;
  Assembler::patch(branch + 0,  31, 12, (upper >> 12) & 0xfffff);               // Lui.
  Assembler::patch(branch + 4,  31, 20, lower & 0xfff);                         // Addiw.
  return LI32_INSTRUCTIONS_NUM * 4;
}

static address get_target_of_li32(address insn_addr) {
  assert_cond(insn_addr != NULL);
  intptr_t target_address = ((int64_t)Assembler::sextract(((unsigned*)insn_addr)[0], 31, 12)) << 12;    // Lui.
  target_address += ((int64_t)Assembler::sextract(((unsigned*)insn_addr)[1], 31, 20));                              // Addiw.
  // sign extend the bit 31 as addiw describes
  target_address  = (((int64_t) target_address) << 32) >> 32;
  return (address)target_address;
}

static address get_target_of_li64(address insn_addr) {
  assert_cond(insn_addr != NULL);
  intptr_t target_address = (((int64_t)Assembler::sextract(((unsigned*)insn_addr)[0], 31, 12)) & 0xfffff) << 44;    // Lui.
  target_address += ((int64_t)Assembler::sextract(((unsigned*)insn_addr)[1], 31, 20)) << 32;                        // Addi.
  target_address += ((int64_t)Assembler::sextract(((unsigned*)insn_addr)[3], 31, 20)) << 20;                        // Addi.
  target_address += ((int64_t)Assembler::sextract(((unsigned*)insn_addr)[5], 31, 20)) << 8;                         // Addi.
  target_address += ((int64_t)Assembler::sextract(((unsigned*)insn_addr)[7], 31, 20));                              // Addi.
  return (address)target_address;
}

static int patch_imm_in_li64(address branch, address target) {
  const int LI64_INSTRUCTIONS_NUM = 8;                                          // lui + addi + slli + addi + slli + addi + slli + addi
  int64_t lower = (intptr_t)target & 0xffffffff;
  lower = lower - ((lower << 44) >> 44);
  int64_t tmp_imm = ((uint64_t)((intptr_t)target & 0xffffffff00000000)) + (uint64_t)lower;
  int32_t upper =  (tmp_imm - (int32_t)lower) >> 32;
  int64_t tmp_upper = upper, tmp_lower = upper;
  tmp_lower = (tmp_lower << 52) >> 52;
  tmp_upper -= tmp_lower;
  tmp_upper >>= 12;
  // Load upper 32 bits. Upper = target[63:32], but if target[31] = 1 or (target[31:28] == 0x7ff && target[19] == 1),
  // upper = target[63:32] + 1.
  Assembler::patch(branch + 0,  31, 12, tmp_upper & 0xfffff);                       // Lui.
  Assembler::patch(branch + 4,  31, 20, tmp_lower & 0xfff);                         // Addi.
  // Load the rest 32 bits.
  Assembler::patch(branch + 12, 31, 20, ((int32_t)lower >> 20) & 0xfff);            // Addi.
  Assembler::patch(branch + 20, 31, 20, (((intptr_t)target << 44) >> 52) & 0xfff);  // Addi.
  Assembler::patch(branch + 28, 31, 20, (intptr_t)target & 0xff);                   // Addi.
  return LI64_INSTRUCTIONS_NUM * 4;
}


#endif //HSDIS_RISCV64_ASSEMBLER_RISCV_HPP
